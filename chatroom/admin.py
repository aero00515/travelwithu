from django.contrib import admin
from chatroom.models import Room, Message

# Register your models here.
@admin.register(Room, Message)
class PersonAdmin(admin.ModelAdmin):
    pass
