from django.contrib.auth.models import User

from rest_framework import serializers
from chatroom import models


class RoomSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.Room
		url_field_name = 'name'

class MessageSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.Message
		# url_field_name = 'room'