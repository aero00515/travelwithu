from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseServerError
from django.views.generic import ListView, DetailView, FormView
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from rest_framework import generics, permissions
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response

from chatroom import serializers
from chatroom.utils.auth import get_login_url
# from chatroom.forms.guest import GuestNameForm
from chatroom.models import Room, Message


class RoomsListView(ListView):
	"""View to show the list of rooms available """
	context_object_name = "rooms"
	template_name = "chatroom/rooms_list.html"
	paginate_by = 20
	model = Room

	# def get_queryset(self):
	# 	filters = {}
	# 	if self.request.user.is_anonymous():
	# 		filters['allow_anonymous_access'] = True
	# 	return Room.objects.filter(**filters)


class RoomView(DetailView):
	"""View for the single room """
	model = Room
	context_object_name = 'room'
	template_name = "chatroom/room.html"


class RoomList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAdminUser,)
	queryset = Room.objects.all()
	serializer_class = serializers.RoomSerializer

class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Room.objects.all()
	serializer_class = serializers.RoomSerializer


class MessageList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Message.objects.all()
	serializer_class = serializers.MessageSerializer

class MessageDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Message.objects.all()
	serializer_class = serializers.MessageSerializer
	# lookup_field = 'room'

@require_POST
@csrf_exempt
def room_do(request):
	try:
		name = request.POST.get('name')
		slug = request.POST.get('slug')
		user = User.objects.get(pk=request.user.pk)
		user2 = User.objects.get(email=request.POST.get('email2'))
		room, created = Room.objects.get_or_create(name=name, slug=slug)
		room.save()
		room.subscribers.add(user, user2)
		room.allow_anonymous_access = False
		room.private = True
		room.save()
		# Messages.objects.create(user=request.user, text=request.POST.get('comment'))

		return HttpResponse(serializers.RoomSerializer(room).data)
	except Exception, e:
		return HttpResponseServerError(str(e))


# class GuestNameView(FormView):
# 	"""Shows the form to choose a guest name to anonymous users """
# 	form_class = GuestNameForm
# 	template_name = 'chatroom/guestname_form.html'

# 	def get_context_data(self, **kwargs):
# 		kwargs.update(super(GuestNameView, self).get_context_data(**kwargs))
# 		room_slug = self.request.GET.get('room_slug')
# 		next = ''
# 		if room_slug:
# 			next = reverse('room_view', kwargs={'slug': room_slug})
# 		kwargs['login_url'] = get_login_url(next)
# 		return kwargs

# 	def get_initial(self):
# 		init = super(GuestNameView, self).get_initial()
# 		room_slug = self.request.GET.get('room_slug')
# 		if room_slug:
# 			init.update(room_slug=room_slug)
# 		return init

# 	def form_valid(self, form):
# 		guest_name = form.cleaned_data.get('guest_name')
# 		room_slug = form.cleaned_data.get('room_slug')
# 		self.request.session['guest_name'] = guest_name
# 		if room_slug:
# 			redirect_url = reverse('room_view', kwargs={'slug': room_slug})
# 		else:
# 			redirect_url = reverse('rooms_list')
# 		return HttpResponseRedirect(redirect_url)