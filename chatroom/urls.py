from django.conf.urls import patterns, url

from chatroom import views
from chatroom.utils.decorators import room_check_access
from chatroom.ajax import chat

urlpatterns = patterns('',
	# room views
	url(r'^rooms/$',
		views.RoomsListView.as_view(),
		name="rooms_list"),
	url(r'^room/(?P<slug>[-\w\d]+)/$',
		room_check_access(views.RoomView.as_view()),
		name="room_view"),
	# url(r'^setguestname/$',
	# 	views.GuestNameView.as_view(),
	# 	name="set_guestname"),

	url(r'^room-do/$', views.room_do, name='room_do'),

	url(r'^api-room/$', 
		views.RoomList.as_view(), 
		name='api-room-list'),
	
	url(r'^api-room/(?P<name>[0-9]+)/$', 
		views.RoomDetail.as_view(), 
		name='api-room-detail'),

	url(r'^api-message/$', 
		views.MessageList.as_view(), 
		name='api-message-list'),
	
	url(r'^api-message/(?P<pk>[0-9]+)/$', 
		views.MessageDetail.as_view(), 
		name='api-message-detail'),

	# ajax requests
	url(r'^get_messages/', chat.ChatView().get_messages),
	url(r'^send_message/', chat.ChatView().send_message),
	url(r'^get_latest_msg_id/', chat.ChatView().get_latest_message_id),
	url(r'^get_users_list/$', chat.ChatView().get_users_list),
	url(r'^notify_users_list/$', chat.ChatView().notify_users_list),
)