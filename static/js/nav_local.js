$(document).ready(function() {
	var is_click_vis = false;
	$('#local_handle').click(function() {
		// Set the effect type
		var effect = 'slide';

		// Set the options for the effect type chosen
		var options = { direction: 'right' };

		// Set the duration (default: 400 milliseconds)
		var duration = 400;

		$('#local_menu').toggle(effect, options, duration);
		is_click_vis = $('#local_menu').is(':visible');
	});

	$(document).scroll(function(event) {
		// console.log('is_click_vis', is_click_vis);
		if ($(document).scrollTop() > 0) {
			// Not at top, dismiss local handle
			if (!is_click_vis) {
				$('#local_menu').toggle(false);
			}
		} else {
			// At top, show local handle
			is_click_vis = false;
			if (!is_click_vis) {
				$('#local_menu').toggle(true);
			}
		}
	});
});