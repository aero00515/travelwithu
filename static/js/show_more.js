$(document).ready(function() {

	function changeAllLinkState(show_all_link, checked) {
		// Change text for showing Hide or View
		show_all_link.data('checked', !checked);
		var temp_text = show_all_link.text();
		show_all_link.text(show_all_link.data('text'));
		show_all_link.data('text', temp_text);
	}

	function checkNextPosition(id, next_show_item_id) {
		var show_all_link = $('a[data-type="show_all_link_' + id + '"]');
		var show_more_link = $('a[data-type="show_more_link_' + id + '"]');

		var checked = show_all_link.data('checked');

		if (next_show_item_id == -1) {
			// To the end
			if (checked) {
				// Already is View All
			} else {
				// Still not checked
				changeAllLinkState(show_all_link, checked);
			}
			show_more_link.hide();
		} else if (next_show_item_id == 3) {
			// At start
			if (checked) {
				// View All Now, turn into give user to View All
				changeAllLinkState(show_all_link, checked);
			} else {
				// Now is already give user to view all
			}
			show_more_link.show();
		} else {
			// NO mind in the middle
			if (checked) {
				// View All Now, turn into give user to View All
				changeAllLinkState(show_all_link, checked);
			} else {
				// Now is already give user to view all
			}
			show_more_link.show();
		}
	}

	$('div[data-type="show_more"]').each(function(index, value) {
		var show_more = $(this);
		var id = show_more.data('pk');
		console.log('show more' + id + ': ', show_more);

		var items = show_more.find('div[data-type="show_more_item"]');
		var next_show_item_id = 3;
		items.each(function(index, value) {
			var show_more_item = $(this);
			var item_id = show_more_item.data('pk');
			console.log('item' + item_id + ': ', show_more_item);

			if (index > 2) {
				// hide the overflow response's response
				show_more_item.hide();
			}
		});

		$('a[data-type="show_all_link_' + id + '"]').click(function() {
			var show_all_link = $(this);
			console.log(id + ': ', show_all_link);
			var checked = show_all_link.data('checked');
			console.log(checked);
			if (checked) {
				// Means already View All, needs to Hide All
				items.each(function(index) {
					if (index > 2) {
						$(this).hide('200');
					}
				});
				next_show_item_id = 3;
			} else {
				// Needs to View All
				items.each(function() {
					$(this).show('200');
				});
				next_show_item_id = -1;
			}

			// changeAllLinkState(show_all_link, checked);
			checkNextPosition(id, next_show_item_id);
		});

		$('a[data-type="show_more_link_' + id + '"]').click(function() {
			var show_more_link = $(this);
			for (var i = 0; i < 3; i++) {
				if (next_show_item_id == items.length) {
					next_show_item_id = -1;
					break;
				}
				var index = next_show_item_id;
				console.log(index + ': ', items.get(index));
				$(items.get(index)).show('200');
				next_show_item_id++;
			}
			checkNextPosition(id, next_show_item_id);
		});
	});

});