$(document).ready(function() {

    $('#fullpage').fullpage({
        // Navigation
        'navigation': true,
        'navigationPosition': 'left',
        'resize':true,
        // 'paddingBottom': getNavbarHeight(),
        // slidesNavigation: true,
        // slidesNavPosition: 'top'
        // anchors: ['systemModel', 'fuzzySystem', 'controller', 'fuzzyController', 'ga', 'lastPage'],
        // anchors: ['Intro', 'Data', 'Demo', 'lastPage'],
        // menu: '#menu',

        // Scrolling
        // loopHorizontal: false,
        // scrollingSpeed: 500,
        // scrollOverflow: true,

        //Design
        // controlArrows: false
        // sectionsColor: ['#f2f2f2', '#4BBFC3', '#7BAABE', 'whitesmoke', '#000', '#666'],
        // sectionsColor: ['#f2f2f2', '#4BBFC3', '#666'],

        // onSlideLeave: function (anchorLink, index, slideIndex, direction) {
        //   console.log('onSlideLeave');
        //   //For the section 1, slide 0 to the right...
        //   if (index == 1 && slideIndex == 0 && direction == 'right') {
        //     console.log('For the section 1, slide 0 to the right...');
        //     var section2 = $('#section1');
        //     var section3 = $('#section2');
        //     $.fn.fullpage.setScrollingSpeed(0);
        //     $.fn.fullpage.scrollSlider(section2, 1);
        //     $.fn.fullpage.scrollSlider(section3, 1);
        //     $.fn.fullpage.setScrollingSpeed(700);
        //   }
        // }
    });
});