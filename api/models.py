from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django_countries.fields import CountryField
from django.db import models
from django.dispatch import receiver

from ckeditor.fields import RichTextField
from easy_thumbnails.files import get_thumbnailer
from image_cropping import ImageRatioField


def content_file_name(instance, filename):
	ext = filename.split(".")[-1]
	return 'user_pic/' + str(instance.user.username) + '.' + ext

## Relation Models
# User
# note: User has traveler and guider, 
# the difference is only at the different action can show.
# 
# - familiar country
# - familiar language
# - familiar geopoint

# Friends
# Messaging
# - Who
# - Group
# - Records

class Language(models.Model):
	name = models.CharField(max_length=254)
	country = CountryField(blank=True)

	def __unicode__(self):
		return "%s/"%self.name + "%s"%self.country


class LanguageLevel(models.Model):
	choice_levels = (
		('A', 'Great'),
		('B', 'Good'),
		('C', 'Soso'),
		('D', 'Bad'),
		('E', 'Terrible')
	)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	language = models.ForeignKey(Language)
	level = models.CharField(max_length=1, choices=choice_levels)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


class Career(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	title = models.CharField(max_length=254)
	detail = models.TextField(blank=True)
	is_current = models.BooleanField(default=True)
	start_at = models.DateTimeField(blank=True)
	end_at = models.DateTimeField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


class Education(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	school = models.CharField(max_length=254)
	department = models.CharField(max_length=254)
	degree = models.CharField(max_length=254)
	is_current = models.BooleanField(default=True)
	start_at = models.DateTimeField(blank=True)
	end_at = models.DateTimeField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


class GoodAt(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	name = models.CharField(max_length=254)
	detail = models.TextField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


class Interest(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	name = models.CharField(max_length=254)
	detail = models.TextField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


## Single Model
# User Extends, finish then start using our service
class UserExtends(models.Model):
	SEX = (
		('M', 'Male'),
		('F', 'Female'),
		('T', 'Third'),
	)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	sex = models.CharField(max_length=1, choices=SEX)
	birthday = models.DateField(blank=True)
	phone = models.CharField(max_length=254, blank=True)
	selfProfile = models.TextField(blank=True)
	country = CountryField(blank=True)
	live_country = CountryField(blank=True)
	contact_info = models.CharField(max_length=254, blank=True)
	available_time = models.CharField(max_length=254, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

# Picture, The user picture stand alone for upload and update
class UserPicture(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	picture = models.ImageField(upload_to=content_file_name, default='/media/user_pic/default.png', null=True, blank=True)
	thumbnail = ImageRatioField('picture', '150x150')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def get_thumbnail_url(self, h, w):
		thumbnail_url = get_thumbnailer(self.picture).get_thumbnail({
			'size': (h, w),
			'box': self.thumbnail,
			'crop': True,
			'detail': True,
		}).url
		return thumbnail_url

# User's credit
class UserCredit(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	traveler_credit = models.IntegerField(default=0)
	guider_credit = models.IntegerField(default=0)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


# class UserSession(models.Model):
# 	user = models.ForeignKey(settings.AUTH_USER_MODEL)
# 	session = models.ForeignKey(Session)  


# Record who gives the credit


# Location, the point on the map
class GeoPoint(models.Model):
	longitude = models.DecimalField(max_digits=35, decimal_places=28)
	latitude = models.DecimalField(max_digits=35, decimal_places=28)
	name = models.CharField(max_length=254)
	describe = RichTextField(blank=True)
	open_time_start = models.DateTimeField(blank=True)
	open_time_end = models.DateTimeField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


class CountryCity(models.Model):
	country_name = models.CharField(max_length=254)
	country_code = models.IntegerField()
	city_name = models.CharField(max_length=254)
	city_alias = models.CharField(max_length=3)
	city_code = models.IntegerField()
	cover_photo = models.ImageField(upload_to='countries')

	def __str__(self):
		return u"%s/"%self.country_name + u"%s"%self.city_name

	class Meta:
		unique_together = ('country_code', 'city_alias',)


class ScheduleItem(models.Model):
	time = models.TimeField()
	detail = models.TextField()
	color = models.CharField(max_length=8)


class Schedule(models.Model):
	schedule_item = models.ManyToManyField(ScheduleItem)
	date = models.DateField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


## Travel System 
# TravelLocal (for Local)
class TravelLocal(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	title = models.CharField(max_length=254)
	geo_point = models.ForeignKey(GeoPoint, null=True, blank=True)
	available_time = models.CharField(max_length=254, blank=True)
	country_city = models.ForeignKey(CountryCity)
	schedule = models.ManyToManyField(Schedule, blank=True)
	detail = RichTextField(blank=True)
	notice = models.TextField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


# TravelInfo (for traveler), for the user who want to ask for advice for their travel info
class TravelInfo(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	time_start = models.DateTimeField(blank=True)
	time_end = models.DateTimeField(blank=True)
	geo_point = models.ForeignKey(GeoPoint, null=True, blank=True)
	country_city = models.ForeignKey(CountryCity)
	detail = models.TextField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

# Q&A
class TravelQuestion(models.Model):
	CATE = (
		('F', 'Food'),
		('D', 'Dress'),
		('L', 'Live'),
		('T', 'Traffic'),
		('E', 'Entertainment'),
		('EL', 'Else'),
	)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	geo_point = models.ForeignKey(GeoPoint, null=True, blank=True)
	country_city = models.ForeignKey(CountryCity)
	rate = models.IntegerField(default=0)
	views = models.IntegerField(default=0)
	title = models.CharField(max_length=254)
	detail = RichTextField(blank=True)
	categories = models.CharField(max_length=2, choices=CATE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

# Travel Local's response (for user)
class TravelLocalResponse(models.Model):
	travel_local = models.ForeignKey(TravelLocal, on_delete=models.CASCADE)
	user = models.ForeignKey(User)
	suggestion = RichTextField(config_name='response', blank=True)
	is_connection = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	# [REF] it throws a django.db.utils.IntegrityError if you try to add a duplicate
	class Meta:
		unique_together = ('travel_local', 'user',)


# Travel Info's response (for guider)
class TravelInfoResponse(models.Model):
	travel_info = models.ForeignKey(TravelInfo, on_delete=models.CASCADE)
	user = models.ForeignKey(User)
	suggestion = RichTextField(config_name='response', blank=True)
	is_connection = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	# [REF] it throws a django.db.utils.IntegrityError if you try to add a duplicate
	class Meta:
		unique_together = ('travel_info', 'user',)

# Travel Question's response (any)
class TravelQuestionResponse(models.Model):
	travel_question = models.ForeignKey(TravelQuestion, on_delete=models.CASCADE)
	user = models.ForeignKey(User)
	detail = RichTextField(blank=True)
	rate = models.IntegerField(default=0)
	is_connection = models.BooleanField(default=True)
	is_choose = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		unique_together = ('travel_question', 'user',)

# Travel Question response's response (any)
class TravelQuestionResponseResponse(models.Model):
	travel_question_response = models.ForeignKey(TravelQuestionResponse, on_delete=models.CASCADE)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	detail = RichTextField(config_name='response', blank=True)
	rate = models.IntegerField(default=0)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


# Sex and the City XDDDDD shit >//<
class HopeCity(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	country = models.CharField(max_length=254)
	city = models.CharField(max_length=254, blank=True)
	more = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return u"%s/"%self.country + u"%s"%self.city


class InviteRelation(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	email = models.CharField(max_length=254, unique=True)
	invite_code = models.CharField(max_length=20)
	is_invited = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return u"%s/"%self.email

