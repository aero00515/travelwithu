from django.conf.urls import patterns, include, url

from api import views

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'travelWithU.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),

	url(r'^notifycount/$', views.notifyCount, name='notify-count'),

	url(r'^users/token/?$', 
		views.obtain_expiring_auth_token, 
		name='user-token'),

	# User
	url(r'^user/$', 
		views.UserList.as_view(), 
		name='user-list'),
	
	url(r'^user/(?P<pk>[0-9]+)/$', 
		views.UserDetail.as_view(), 
		name='user-detail'),

	url(r'^userextends/$', 
		views.UserExtendsList.as_view(), 
		name='user-extends-list'),
	
	url(r'^userextends/(?P<user>[0-9]+)/$', 
		views.UserExtendsDetail.as_view(), 
		name='user-extends-detail'),

	url(r'^userpicture/$', 
		views.UserPictureList.as_view(), 
		name='user-picture-list'),
	
	url(r'^userpicture/(?P<user>[0-9]+)/$', 
		views.UserPictureDetail.as_view(), 
		name='user-picture-detail'),

	url(r'^usercredit/$', 
		views.UserCreditList.as_view(), 
		name='user-credit-list'),
	
	url(r'^usercredit/(?P<user>[0-9]+)/$', 
		views.UserCreditDetail.as_view(), 
		name='user-credit-detail'),

	url(r'^geopoint/$', 
		views.GeoPointList.as_view(), 
		name='geo-point-list'),
	
	url(r'^geopoint/(?P<pk>[0-9]+)/$', 
		views.GeoPointDetail.as_view(), 
		name='geo-point-detail'),

	## Travel System
	# Travel
	url(r'^travelinfo/$', 
		views.TravelInfoList.as_view(), 
		name='travel-info-list'),
	
	url(r'^travelinfo/(?P<pk>[0-9]+)/$', 
		views.TravelInfoDetail.as_view(), 
		name='travel-info-detail'),

	url(r'^travellocal/$', 
		views.TravelLocalList.as_view(), 
		name='travel-local-list'),
	
	url(r'^travellocal/(?P<pk>[0-9]+)/$', 
		views.TravelLocalDetail.as_view(), 
		name='travel-local-detail'),

	url(r'^travelquestion/$', 
		views.TravelQuestionList.as_view(), 
		name='travel-question-list'),
	
	url(r'^travelquestion/(?P<pk>[0-9]+)/$', 
		views.TravelQuestionDetail.as_view(), 
		name='travel-question-detail'),

	# Response
	url(r'^travelinforesponse/$', 
		views.TravelInfoResponseList.as_view(), 
		name='travel-info-response-list'),
	
	url(r'^travelinforesponse/(?P<pk>[0-9]+)/$', 
		views.TravelInfoResponseDetail.as_view(), 
		name='travel-info-response-detail'),

	url(r'^travellocalresponse/$', 
		views.TravelLocalResponseList.as_view(), 
		name='travel-local-response-list'),
	
	url(r'^travellocalresponse/(?P<pk>[0-9]+)/$', 
		views.TravelLocalResponseDetail.as_view(), 
		name='travel-local-response-detail'),

	url(r'^travelquestionresponse/$', 
		views.TravelQuestionResponseList.as_view(), 
		name='travel-question-response-list'),
	
	url(r'^travelquestionresponse/(?P<pk>[0-9]+)/$', 
		views.TravelQuestionResponseDetail.as_view(), 
		name='travel-question-response-detail'),

	# Response Response
	url(r'^travelquestionresponseresponse/$', 
		views.TravelQuestionResponseResponseList.as_view(), 
		name='travel-question-response-response-list'),
	
	url(r'^travelquestionresponseresponse/(?P<pk>[0-9]+)/$', 
		views.TravelQuestionResponseResponseDetail.as_view(), 
		name='travel-question-response-response-detail'),

	# Room & Message
	url(r'^room/$', 
		views.RoomList.as_view(), 
		name='room-list'),
	
	url(r'^room/(?P<pk>[0-9]+)/$', 
		views.RoomDetail.as_view(), 
		name='room-detail'),

	url(r'^message/$', 
		views.MessageList.as_view(), 
		name='message-list'),
	
	url(r'^message/(?P<pk>[0-9]+)/$', 
		views.MessageDetail.as_view(), 
		name='message-detail'),

	# Sex and the City
	url(r'^hopecity/$', 
		views.HopeCityList.as_view(), 
		name='hopecity-list'),
	
	url(r'^hopecity/(?P<pk>[0-9]+)/$', 
		views.HopeCityDetail.as_view(), 
		name='hopecity-detail'),
)
