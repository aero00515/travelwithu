from django.contrib.auth.models import User

from rest_framework import serializers
from api import models
from travel_messenger.models import Room, Message

# User's
class UserSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = User
		fields = ('id', 'username','password', 'first_name', 'email', 'last_login')
		write_only_fields = ('password',)

	def restore_object(self, attrs, instance=None):
		# call set_password on user object. Without this
		# the password will be stored in plain text.
		user = super(UserSerializer, self).restore_object(attrs, instance)
		user.set_password(attrs['password'])
		return user

class UserExtendsSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.UserExtends
		# fields = ('id', 'user', 'sex', 'birthday', 'phone', 
		# 'country', 'contact_info', 'available_time', 'created_at', 'updated_at')
		url_field_name = 'user'

class LimitUserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = ('id', 'first_name')

class UserPictureSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.UserPicture
		# fields = ('id', 'user', 'picture', 'thumbnail')
		url_field_name = 'user'

class UserCreditSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.UserCredit
		url_field_name = 'user'

class GeoPointSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.GeoPoint
		# fields = ('id', 'longitude', 'latitude', 'name', 'describe',
		# 'open_time_start', 'open_time_end',  'created_at', 'updated_at')

# Travel System's
class TravelInfoSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.TravelInfo

class TravelLocalSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.TravelLocal

class TravelQuestionSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.TravelQuestion

class TravelInfoResponseSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.TravelInfoResponse

class TravelLocalResponseSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.TravelLocalResponse

class TravelQuestionResponseSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.TravelQuestionResponse

class TravelQuestionResponseResponseSerializer(serializers.ModelSerializer):
	class Meta:
		model = models.TravelQuestionResponseResponse
		url_field_name = 'travel_question_response'

# Messages
class RoomSerializer(serializers.ModelSerializer):
	class Meta:
		model = Room
		# url_field_name = 'travel_question_response'

class MessageUserSerializer(serializers.ModelSerializer):
	user = LimitUserSerializer()
	created_at = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

	class Meta:
		model = Message
		fields = ('id', 'user', 'room', 'text', 'pending', 'created_at')

class MessageSerializer(serializers.ModelSerializer):
	class Meta:
		model = Message

# Sex and the City
class HopeCitySerializer(serializers.ModelSerializer):
	class Meta:
		model = models.HopeCity

