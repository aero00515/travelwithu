import datetime

from django.conf import settings

from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication

class ExpiringTokenAuthentication(TokenAuthentication):
	def authenticate_credentials(self, key):
		try:
			token = self.model.objects.get(key=key)
		except self.model.DoesNotExist:
			raise exceptions.AuthenticationFailed('Invalid token')

		if not token.user.is_active:
			raise exceptions.AuthenticationFailed('User inactive or deleted')

		# This is required for the time comparison
		utc_now = datetime.utcnow()

		if token.created < utc_now - datetime.timedelta(hours=settings.ACCESS_TOKEN_EXPIRE_HOUR):
			raise exceptions.AuthenticationFailed('Token has expired')

		return token.user, token