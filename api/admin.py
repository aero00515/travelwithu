from django.contrib import admin
from image_cropping import ImageCroppingMixin
from api.models import UserExtends, GeoPoint, UserPicture, UserCredit, CountryCity, TravelInfo, TravelQuestion, TravelInfoResponse, TravelQuestionResponse, TravelQuestionResponseResponse, HopeCity, Language, LanguageLevel, Career, Interest, ScheduleItem, Schedule, TravelLocal, Education, GoodAt, InviteRelation

# Register your models here.
@admin.register(UserExtends, GeoPoint, UserPicture, UserCredit, CountryCity, TravelInfo, TravelQuestion, TravelInfoResponse, TravelQuestionResponse, TravelQuestionResponseResponse, HopeCity, Language, LanguageLevel, Career, Interest, ScheduleItem, Schedule, TravelLocal, Education, GoodAt, InviteRelation)
class PersonAdmin(ImageCroppingMixin, admin.ModelAdmin):
    pass
