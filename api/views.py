from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.decorators.http import require_GET

from rest_framework import generics, permissions
from rest_framework.authentication import BasicAuthentication
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response

from api import serializers
from api.models import UserExtends, UserPicture, UserCredit, GeoPoint, TravelInfo, TravelQuestion, TravelInfoResponse, TravelQuestionResponse, TravelQuestionResponseResponse, HopeCity, TravelLocal, TravelLocalResponse
from api.permissions import IsStaffOrTargetUser, IsStaffOrTargetUserOrReadOnly, IsOwner

from travel_messenger.models import Room, Message


class ObtainExpiringAuthToken(ObtainAuthToken):
	def post(self, request):
		serializer = self.serializer_class(data=request.DATA)
		if serializer.is_valid():
			token, created =  Token.objects.get_or_create(user=serializer.object['user'])

			if not created:
				token.delete()
				token = Token.objects.create(user=serializer.object['user'])
				token.created = datetime.datetime.utcnow()
				token.save()

			return Response({'token': token.key})
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

obtain_expiring_auth_token = ObtainExpiringAuthToken.as_view()

@require_GET
def notifyCount(request):
	user_rooms = Room.objects.filter(subscribers__exact=request.user)
	msgs = Message.objects.filter(room__in=user_rooms, pending=True).exclude(user=request.user)
	return HttpResponse(len(msgs));


## User
class UserList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAdminUser,)
	queryset = User.objects.all()
	serializer_class = serializers.UserSerializer

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (IsStaffOrTargetUser,)
	queryset = User.objects.all()
	serializer_class = serializers.UserSerializer


class UserExtendsList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAdminUser,)
	queryset = UserExtends.objects.all()
	serializer_class = serializers.UserExtendsSerializer

class UserExtendsDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (IsStaffOrTargetUser,)
	queryset = UserExtends.objects.all()
	serializer_class = serializers.UserExtendsSerializer
	lookup_field = 'user'


class UserPictureList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = UserPicture.objects.all()
	serializer_class = serializers.UserPictureSerializer

class UserPictureDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (IsStaffOrTargetUserOrReadOnly,)
	queryset = UserPicture.objects.all()
	serializer_class = serializers.UserPictureSerializer
	lookup_field = 'user'

	def get(self, request, user, format=None):
		# h = request.QUERY_PARAMS.get('h', '48')
		# w = request.QUERY_PARAMS.get('w', '48')
		try:
			userPicture = UserPicture.objects.get(user=user)
			userPicture.thumbnail = userPicture.get_thumbnail_url(48, 48)
			return Response(serializers.UserPictureSerializer(userPicture).data)
		except UserPicture.DoesNotExist:
			context = {"detail": "Not found"}
			return Response(context)


class UserCreditList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAdminUser,)
	queryset = UserCredit.objects.all()
	serializer_class = serializers.UserCreditSerializer

class UserCreditDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (IsStaffOrTargetUserOrReadOnly,)
	queryset = UserCredit.objects.all()
	serializer_class = serializers.UserCreditSerializer
	lookup_field = 'user'


class GeoPointList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = GeoPoint.objects.all()
	serializer_class = serializers.GeoPointSerializer

class GeoPointDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = GeoPoint.objects.all()
	serializer_class = serializers.GeoPointSerializer


## Travel System
# Travel Q&A and Infos
class TravelInfoList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = TravelInfo.objects.all()
	serializer_class = serializers.TravelInfoSerializer

class TravelInfoDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, IsStaffOrTargetUserOrReadOnly,)
	queryset = TravelInfo.objects.all()
	serializer_class = serializers.TravelInfoSerializer


class TravelLocalList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = TravelLocal.objects.all()
	serializer_class = serializers.TravelLocalSerializer

class TravelLocalDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, IsStaffOrTargetUserOrReadOnly)
	queryset = TravelLocal.objects.all()
	serializer_class = serializers.TravelLocalSerializer


class TravelQuestionList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = TravelQuestion.objects.all()
	serializer_class = serializers.TravelQuestionSerializer

class TravelQuestionDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, IsStaffOrTargetUserOrReadOnly)
	queryset = TravelQuestion.objects.all()
	serializer_class = serializers.TravelQuestionSerializer

# Travel Responses
class TravelInfoResponseList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = TravelInfoResponse.objects.all()
	serializer_class = serializers.TravelInfoResponseSerializer

class TravelInfoResponseDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, IsStaffOrTargetUserOrReadOnly)
	queryset = TravelInfoResponse.objects.all()
	serializer_class = serializers.TravelInfoResponseSerializer


class TravelLocalResponseList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = TravelLocalResponse.objects.all()
	serializer_class = serializers.TravelLocalResponseSerializer

class TravelLocalResponseDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, IsStaffOrTargetUserOrReadOnly)
	queryset = TravelLocalResponse.objects.all()
	serializer_class = serializers.TravelLocalResponseSerializer


class TravelQuestionResponseList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = TravelQuestionResponse.objects.all()
	serializer_class = serializers.TravelQuestionResponseSerializer

class TravelQuestionResponseDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, IsStaffOrTargetUserOrReadOnly)
	queryset = TravelQuestionResponse.objects.all()
	serializer_class = serializers.TravelQuestionResponseSerializer


# Travel Response's Response
class TravelQuestionResponseResponseList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = TravelQuestionResponseResponse.objects.all()
	serializer_class = serializers.TravelQuestionResponseResponseSerializer

class TravelQuestionResponseResponseDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated, IsStaffOrTargetUserOrReadOnly)
	queryset = TravelQuestionResponseResponse.objects.all()
	serializer_class = serializers.TravelQuestionResponseResponseSerializer
	# lookup_field = 'travel_question_response'


# Room & Messages
class RoomList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Room.objects.all()
	serializer_class = serializers.RoomSerializer

class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Room.objects.all()
	serializer_class = serializers.RoomSerializer

class MessageList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		room_id = self.request.QUERY_PARAMS.get('room_id', None)
		pending = self.request.QUERY_PARAMS.get('pending', None)

		if room_id != None:
			if pending != None:
				if pending == 'false':
					pending = False
				else:
					pending = True
				queryset = Message.objects.filter(room__id=room_id, pending=pending).exclude(user=self.request.user)
			else:
				queryset = Message.objects.filter(room__id=room_id)
		else:
			queryset = Message.objects.all()
		return queryset

	def get_serializer_class(self):
		format_username = self.request.QUERY_PARAMS.get('name', 'False')
		if format_username == 'True':
			serializer_class = serializers.MessageUserSerializer
		else:
			serializer_class = serializers.MessageSerializer
		return serializer_class

class MessageDetail(generics.RetrieveAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Message.objects.all()
	serializer_class = serializers.MessageSerializer

# Sex and the City
class HopeCityList(generics.ListCreateAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = HopeCity.objects.all()
	serializer_class = serializers.HopeCitySerializer

class HopeCityDetail(generics.RetrieveAPIView):
	# authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	queryset = HopeCity.objects.all()
	serializer_class = serializers.HopeCitySerializer
