from django.db import models
from django.contrib.auth.models import User


## User connection
# Connection room
class Room(models.Model):
	name = models.CharField(max_length=254, blank=True)
	subscribers = models.ManyToManyField(User, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField()


# User comments
class Message(models.Model):
	user = models.ForeignKey(User)
	room = models.ForeignKey(Room)
	text = models.TextField(blank=True)
	pending = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)

