import redis
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.shortcuts import render, redirect

from account.utils import user_display
from api.serializers import MessageSerializer
from api.models import TravelInfoResponse, TravelLocalResponse
from travel_messenger.models import Message, Room

import json
import datetime


def custom_redirect(url_name, *args, **kwargs):
	from django.core.urlresolvers import reverse 
	import urllib
	url = reverse(url_name, args = args)
	params = urllib.urlencode(kwargs)
	return HttpResponseRedirect(url + "?%s" % params)


@csrf_exempt
def node_api(request):
	try:
		# Get User from sessionid
		session = Session.objects.get(session_key=request.POST.get('sessionid'))
		user_id = session.get_decoded().get('_auth_user_id')
		user = User.objects.get(pk=user_id)
		# user = request.user
		post_user_id = int(request.POST.get('current_user_id'))
		if post_user_id != user.pk:
			error = 'User miss: ' + str(post_user_id) + ', ' + str(user.pk)
			return HttpResponseServerError(error)

		# Get to user
		to_user_id = request.POST.get('to_user_id')
		to_user = User.objects.get(pk=to_user_id)

		# TODO make sure the room is created 
		# 	with the user in subscribers
		# P.S. room should be created with both connected
		# 	(trigger by travel response or user's request)

		# Get room
		room_id = request.POST.get('room_id')
		room = Room.objects.get(pk=room_id)
		today = datetime.datetime.today()
		room.updated_at = today
		room.save()

		# Get message
		comment = request.POST.get('comment')
 
		# Create comment
		msg = Message.objects.create(user=user, room=room, text=comment)
		chat_data = MessageSerializer(msg).data
		chat_data['current_user_id'] = user.pk
		chat_data['to_user_id'] = to_user_id
		chat_data['first_name'] = user.first_name
		chat_data['created_at'] = msg.created_at.strftime("%Y-%m-%d %H:%M")

		# Once comment has been created post it to the chat channel
		r = redis.StrictRedis(host='localhost', port=6379, db=0)
		r.publish('chat', json.dumps(chat_data))

		return HttpResponse("Everything worked :)")
	except Exception, e:
		return HttpResponseServerError(str(e))


def request_to_msg(request):
	user = request.user
	user_to_req = request.GET.get('req_msg_to')
	res_pk = request.GET.get('res_pk')
	resType = str(request.GET.get('type'))
	user_to = User.objects.get(pk=user_to_req)
	rooms = Room.objects.filter(subscribers__exact=user).filter(subscribers__exact=user_to)
	if rooms:
		# has this, show to this room
		room = rooms[0]
	else:
		is_connection = False
		if resType == 'info':
			travelInfoResponse = TravelInfoResponse.objects.get(pk=res_pk)
			# if travelInfoResponse.travel_info.user is user:
			is_connection = travelInfoResponse.is_connection
		elif resType == 'local':
			travelLocalResponse = TravelLocalResponse.objects.get(pk=res_pk)
			is_connection = travelLocalResponse.is_connection

		if is_connection:
			# don't hav, create room
			name = user.first_name + '_' + user_to.first_name
			room = Room(name=name, updated_at=datetime.datetime.now())
			room.save()
			room.updated_at = datetime.datetime.now()
			room.subscribers.add(user)
			room.subscribers.add(user_to)
			room.save()
		else:
			return redirect('home:chooser')
			# return HttpResponse("is not " + str(is_connection) + " " + str(res_pk) + " " + resType)


	# return custom_redirect('message:index', room=rooms.pk)
	typo = '?room=' + str(room.pk)
	response = redirect('message:index')
	response['Location'] += typo
	return response


class Index(generic.TemplateView):

	template_name = 'travel_messenger/index.html'

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			user_display(request.user)
			return super(Index, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Index, self).get_context_data(**kwargs)
		# if self.request.user.is_authenticated:
		# warnings.simplefilter('error', DeprecationWarning)
		# context['users'] = User.objects.all()
		rooms = Room.objects.filter(subscribers__exact=self.request.user).order_by('-updated_at')
		context['rooms'] = rooms

		room_id = self.request.GET.get('room')
		if room_id:
			room = Room.objects.get(pk = int(room_id))
			context['messages'] = Message.objects.filter(room=room)
			context['room_id'] = int(room_id)
		else:
			if rooms:
				context['messages'] = Message.objects.filter(room=rooms[0])
		return context
