from django.conf.urls import patterns, include, url

from travel_messenger import views

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'travelWithU.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),
	url(r'^$', views.Index.as_view(), name="index"),
	url(r'^req_msg/$', views.request_to_msg, name="req_to_msg"),
	url(r'^node_api/$', views.node_api, name='node_api'),
)
