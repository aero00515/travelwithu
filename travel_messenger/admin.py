from django.contrib import admin
from travel_messenger import models

# Register your models here.
@admin.register(models.Message, models.Room)
class MessageAdmin(admin.ModelAdmin):
    pass
