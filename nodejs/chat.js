var this_server_port = 4000;
var remote_server = '127.0.0.1';
var remote_server_port = 8000;
var remote_server_path = '/messages/node_api/';

var http = require('http');
var server = http.createServer().listen(this_server_port);
var io = require('socket.io').listen(server);
var cookie_reader = require('cookie');
var querystring = require('querystring');

var redis = require('redis');
var sub = redis.createClient();

// var ids = [];
// Save the sockets for rooms
var users = {};
var notify = {};

//Subscribe to the Redis chat channel
sub.subscribe('chat');

//Configure socket.io to store cookie set by Django
// io.use(function(){
// });

// API for no namespace:
// var clients = io.sockets.clients();
// var clients = io.sockets.clients('room'); // all users from room 'room'

// For a namespace
// var clients = io.of('/chat').clients();
// var clients = io.of('/chat').clients('room'); // all users from room 'room'

//Grab message from Redis and send to client
sub.on('message', function(channel, message) {
	// message move to here because message will add once after refresh
	var j_m = JSON.parse(message);
	var to_user_id = j_m['to_user_id'];
	var current_user_id = j_m['current_user_id'];
	console.log('[message] to: ' + to_user_id);
	console.log('[message] cu: ' + current_user_id);
	// console.log('[message] users: ', users);
	// Judge the user's socket
	if ((typeof users[to_user_id]) !== "undefined") {
		console.log('[message] send to' + to_user_id);
		var ss = users[to_user_id];
		ss.forEach(function(s) {
			console.log('[message] socket id: ' + s.id);
			s.send(message);
		});
		// users[to_user_id].send(message);
	} else {
		// User not online
		console.log('[message] User not online');
	}
	if ((typeof users[current_user_id]) !== "undefined") {
		console.log('[message] send self ' + current_user_id);
		var ss = users[current_user_id];
		ss.forEach(function(s) {
			console.log('[message] socket id: ' + s.id);
			s.send(message);
		});
		// users[current_user_id].send(message);
	} else {
		// User cannot get this socket
		console.log('[message] User cannot get this socket');
	}

	// Send notify if user online
	if ((typeof notify[to_user_id]) !== "undefined") {
		console.log('[notify] send to ' + to_user_id);
		var ns = notify[to_user_id];
		ns.forEach(function(s) {
			console.log('[notify] socket id: ' + s.id);
			s.emit('notifier', 'update now baby.');
		});
	} else {
		// User not online
		console.log('[notify] User not online');
	}

});

// io.use(function () {
	
// });

//Configure socket.io to store cookie set by Django
io.set('authorization', function(data, accept) {
	console.log('auth');
	if(data.headers.cookie) {
		data.cookie = cookie_reader.parse(data.headers.cookie);
		return accept(null, true);
	}
	return accept('error', false);
});
// io.set('log level', 1);

io.sockets.on('connection', function (socket) {

	socket.on('notify', function(id, callback) {
		console.log('[notify] reg. ' + id);
		var ns = [];
		socket.user_id = id;
		if (id in notify) {
			// allow second connection of same user different login
			console.log('[notify] already hav user');
			ns = notify[socket.user_id];
		} else {
			console.log('[notify] ' + true);
		}
		ns.push(socket);
		notify[socket.user_id] = ns;
		callback(true);
	});

	socket.on('online user', function(data, callback) {
		// check user exists or not, for message go through the correct socket
		console.log('[online] ' + data);
		var ss = [];
		if (data in users) {
			// allow second connection of same user different login
			console.log('[online] already hav user');
			socket.user_id = data;
			ss = users[socket.user_id];
			ss.push(socket);
			users[socket.user_id] = ss;
			callback(true);
		} else {
			console.log('[online] ' + true);
			socket.user_id = data;
			ss.push(socket);
			users[socket.user_id] = ss;
			// console.log(socket.user_id);
			// console.log('[online] users: ', users);
			callback(true);
		}
	});

	socket.on('error', function (err) { 
		console.error(err.stack);
		// TODO, cleanup 
	});

	//Client is sending message through socket.io
	socket.on('send_message', function (message) {
		var json_message = JSON.parse(message);
		values = querystring.stringify({
			'comment': json_message.comment,
			'current_user_id': json_message.current_user_id,
			'to_user_id': json_message.to_user_id,
			'room_id': json_message.room_id,
			'sessionid': socket.request.cookie.sessionid
		});

		// console.log('message: ' + message);
		// console.log('cookie: ' + socket.request.cookie);
		// console.log('cookie: ' + socket.request.cookie.csrftoken);
		// console.log('h cookie: ' + socket.request.headers.cookie);
		// console.log('session: ' + socket.request.cookie.sessionid);

		// var token = socket.handshake.headers.cookie['token'];

		var options = {
			host: remote_server,
			port: remote_server_port,
			path: remote_server_path,
			method: 'POST',
			headers: {
				// "Authorization": "Token " + token,
				// 'Cookie': socket.request.headers.cookie,
				'Content-Type': 'application/x-www-form-urlencoded',
				'Content-Length': values.length
			}
		};

		//Send message to Django server
		var req = http.get(options, function(res) {
			res.setEncoding('utf8');

			//Print out error message
			res.on('data', function(message) {
				// console.log('Message: ' + message);
				if(message != 'Everything worked :)') {
					console.log('Message: ' + message);
				}
			});
		});

		req.write(values);
		req.end();
	});

	socket.on('disconnect', function(data) {
		console.log('[disconnect] ' + socket.user_id);
		if(!socket.user_id) {
			return;
		}
		if ((typeof users[socket.user_id]) !== "undefined") {
			console.log('[--user]' + socket.user_id);
			var ss = users[socket.user_id];
			var index = ss.indexOf(socket);
			if (index > -1) {
				ss.splice(index, 1);
			}
			users[socket.user_id] = ss;
		}

		if ((typeof notify[socket.user_id]) !== "undefined") {
			console.log('[--notify]' + socket.user_id);
			var ns = notify[socket.user_id];
			var index = ns.indexOf(socket);
			if (index > -1) {
				ns.splice(index, 1);
			}
			notify[socket.user_id] = ns;
		}
	});
});
