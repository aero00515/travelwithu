import unittest
from django.core.urlresolvers import reverse
from django.test import Client

class ApiTravelInfoTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_details(self):
        # Issue a GET request.
        url = reverse('api:travel-info-list')
        response = self.client.get(url)

        # Check that the response is 403 OK.
        # Cuz not login yet
        self.assertEqual(response.status_code, 403)

        # Check that the rendered context contains 5 customers.
        # self.assertEqual(len(response.context['customers']), 5)