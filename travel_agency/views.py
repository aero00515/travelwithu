from django.views import generic
from django.views.generic.edit import FormView, CreateView, UpdateView
# from django.views.generic.edit import UpdateView, DeleteView
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.db.models import Count
from django.shortcuts import redirect

# from account.utils import user_display

from rest_framework.decorators import api_view
# authentication_classes

# from api import models
# from api.auth import ExpiringTokenAuthentication
from api.models import UserExtends, UserPicture, CountryCity, TravelInfo, TravelQuestion, TravelInfoResponse, TravelQuestionResponse, TravelQuestionResponseResponse, TravelLocal, TravelLocalResponse

from travel_agency import forms

import warnings



# def user_logged_in_handler(sender, request, user, **kwargs):
# 	UserSession.objects.get_or_create(
# 		user = user, session_id = request.session.session_key
# 	)


# def delete_user_sessions(user):
# 	user_sessions = UserSession.objects.filter(user = user)
# 	for user_session in user_session:
# 		user_session.session.delete()


# user_logged_in.connect(user_logged_in_handler)


class Chooser(generic.TemplateView):
	template_name = 'travel_agency/chooser.html'

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			return super(Chooser, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Chooser, self).get_context_data(**kwargs)
		context['countries'] = CountryCity.objects.all()
		return context


class Home(generic.TemplateView):
	template_name = 'travel_agency/home.html'

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		elif not request.GET.get('country', None):
			return redirect('home:chooser')
		else:
			# user_display(request.user)
			return super(Home, self).dispatch(request, *args, **kwargs)

	## Test user messaging, [remove when publish]
	def get_context_data(self, **kwargs):
		context = super(Home, self).get_context_data(**kwargs)
		# if self.request.user.is_authenticated:
		# warnings.simplefilter('error', DeprecationWarning)
		get_country_city = self.request.GET.get('country', None)
		cc = get_country_city.split("-")
		country_city = CountryCity.objects.get(country_code=int(cc[0]), city_alias=cc[1])

		travel_locals = TravelLocal.objects.filter(country_city=country_city).annotate(res_count=Count('travellocalresponse'))
		travel_infos = TravelInfo.objects.filter(country_city=country_city).annotate(res_count=Count('travelinforesponse'))
		travel_questions = TravelQuestion.objects.filter(country_city=country_city).annotate(res_count=Count('travelquestionresponse'))

		context['users'] = User.objects.all()
		context['travel_locals'] = travel_locals
		context['travel_infos'] = travel_infos
		context['travel_questions'] = travel_questions
		context['country'] = self.request.GET.get('country', None)
		return context

class MainTravelInfoView(generic.TemplateView):
	template_name = 'travel_agency/travel_infos/_main_travel_info_home.html'

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			# user_display(request.user)
			return super(MainTravelInfoView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(MainTravelInfoView, self).get_context_data(**kwargs)

		travel_info_id = self.request.GET.get('travel_info_id', None)
		travel_info = TravelInfo.objects.get(pk=travel_info_id)
		travel_info_responses = TravelInfoResponse.objects.filter(travel_info=travel_info).order_by('created_at')

		context['travel_info'] = travel_info
		context['travel_info_responses'] = travel_info_responses
		# context['travel_info_response_form'] = forms.TravelInfoResponseForm

		return context


class MainTravelLocalView(generic.TemplateView):
	template_name = 'travel_agency/travel_locals/_main_travel_local_home.html'

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			# user_display(request.user)
			return super(MainTravelLocalView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(MainTravelLocalView, self).get_context_data(**kwargs)

		travel_local_id = self.request.GET.get('travel_local_id', None)
		travel_local = TravelLocal.objects.get(pk=travel_local_id)
		travel_local_responses = TravelLocalResponse.objects.filter(travel_local=travel_local).order_by('created_at')

		context['travel_local'] = travel_local
		context['travel_local_responses'] = travel_local_responses
		# context['travel_info_response_form'] = forms.TravelInfoResponseForm

		return context


class MainTravelQuestionView(generic.TemplateView):
	template_name = 'travel_agency/travel_questions/_main_travel_question_home.html'

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			# user_display(request.user)
			return super(MainTravelQuestionView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(MainTravelQuestionView, self).get_context_data(**kwargs)

		travel_question_id = self.request.GET.get('travel_question_id', None)
		travel_question = TravelQuestion.objects.get(pk=travel_question_id)
		travel_question_responses = TravelQuestionResponse.objects.filter(travel_question=travel_question).order_by('created_at')
		travel_question_res_res_s = TravelQuestionResponseResponse.objects.filter(
			travel_question_response__in=travel_question_responses
		).order_by('created_at')

		res_res_s = {}
		for res in travel_question_responses:
			res_res_s[res.pk] = (TravelQuestionResponseResponse.objects.filter(travel_question_response=res).order_by('created_at'))

		context['travel_question'] = travel_question
		context['travel_question_responses'] = travel_question_responses
		context['travel_question_res_res_s'] = res_res_s
		# context['travel_question_response_form'] = forms.TravelQuestionResponseForm
		# context['travel_question_res_res_form'] = forms.TravelQuestionResponseResponseForm

		return context

# Info, Question, Local Create View
class TravelQuestionView(CreateView):
	template_name = 'travel_agency/form_views/travel_question_form.html'
	form_class = forms.TravelQuestionForm

	def get_context_data(self, **kwargs):
		context = super(TravelQuestionView, self).get_context_data(**kwargs)
		context['country'] = self.request.GET.get('country', None)
		return context

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		# elif not request.GET.get('country', None):
		# 	return redirect('home:chooser')
		else:
			return super(TravelQuestionView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			# reverse('edit_project', kwargs={'project_id':4})
			country = "country=" + self.request.POST.get('country', None)
			return "%s?%s" % (reverse('home:home'), country) # what url you wish to return

	def form_valid(self, form):
		country = self.request.POST.get('country', None)
		cc = country.split("-")
		country_city = CountryCity.objects.get(country_code=int(cc[0]), city_alias=cc[1])

		travelQuestion = form.save(commit=False)
		travelQuestion.user = self.request.user
		travelQuestion.country_city = country_city
		travelQuestion.save()
		return super(TravelQuestionView, self).form_valid(form)


class TravelInfoView(CreateView):
	template_name = 'travel_agency/form_views/travel_info_form.html'
	form_class = forms.TravelInfoForm

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		# elif not request.GET.get('country', None):
		# 	return redirect('home:chooser')
		else:
			return super(TravelInfoView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(TravelInfoView, self).get_context_data(**kwargs)
		context['country'] = self.request.GET.get('country', None)
		return context

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			country = "country=" + self.request.POST.get('country', None)
			return "%s?%s" % (reverse('home:home'), country) # what url you wish to return

	def form_valid(self, form):
		country = self.request.POST.get('country', None)
		cc = country.split("-")
		country_city = CountryCity.objects.get(country_code=int(cc[0]), city_alias=cc[1])

		travelInfo = form.save(commit=False)
		travelInfo.user = self.request.user	
		travelInfo.country_city = country_city
		travelInfo.save()
		return super(TravelInfoView, self).form_valid(form)


class TravelLocalView(CreateView):
	template_name = 'travel_agency/form_views/travel_local_form.html'
	form_class = forms.TravelLocalForm

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		# elif not request.GET.get('country', None):
		# 	return redirect('home:chooser')
		else:
			return super(TravelLocalView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(TravelLocalView, self).get_context_data(**kwargs)
		context['country'] = self.request.GET.get('country', None)
		return context

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			country = "country=" + self.request.POST.get('country', None)
			return "%s?%s" % (reverse('home:home'), country) # what url you wish to return

	def form_valid(self, form):
		country = self.request.POST.get('country', None)
		cc = country.split("-")
		country_city = CountryCity.objects.get(country_code=int(cc[0]), city_alias=cc[1])

		travelLocal = form.save(commit=False)
		travelLocal.user = self.request.user	
		travelLocal.country_city = country_city
		travelLocal.save()
		return super(TravelLocalView, self).form_valid(form)


# Info, Question, Local Update View
class TravelQuestionUpdateView(UpdateView):
	model = TravelQuestion
	template_name = 'travel_agency/form_views/travel_question_update_form.html'
	form_class = forms.TravelQuestionForm

	def dispatch(self, request, *args, **kwargs):
		tq = self.get_object()
		if not request.user.is_authenticated():
			return redirect('intro')
		elif tq.user != self.request.user:
			return redirect('home:chooser')
		else:
			return super(TravelQuestionUpdateView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			return reverse('home:chooser') # what url you wish to return


class TravelInfoUpdateView(UpdateView):
	model = TravelInfo
	template_name = 'travel_agency/form_views/travel_info_update_form.html'
	form_class = forms.TravelInfoForm

	def dispatch(self, request, *args, **kwargs):
		ti = self.get_object()
		if not request.user.is_authenticated():
			return redirect('intro')
		elif ti.user != self.request.user:
			return redirect('home:chooser')
		else:
			return super(TravelInfoUpdateView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			return reverse('home:chooser') # what url you wish to return


class TravelLocalUpdateView(UpdateView):
	model = TravelLocal
	template_name = 'travel_agency/form_views/travel_local_update_form.html'
	form_class = forms.TravelLocalForm

	def dispatch(self, request, *args, **kwargs):
		tl = self.get_object()
		if not request.user.is_authenticated():
			return redirect('intro')
		elif tl.user != self.request.user:
			return redirect('home:chooser')
		else:
			return super(TravelLocalUpdateView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			return reverse('home:chooser') # what url you wish to return


# Deprecated
# Responses form view
class TravelInfoResponseView(FormView):
	template_name = 'travel_agency/form_views/travel_info_response_form.html'
	form_class = forms.TravelInfoResponseForm

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			return super(TravelInfoResponseView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			return reverse('home:chooser') # what url you wish to return


class TravelQuestionResponseView(FormView):
	template_name = 'travel_agency/form_views/travel_question_response_form.html'
	form_class = forms.TravelQuestionResponseForm

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			return super(TravelQuestionResponseView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			return reverse('home:chooser') # what url you wish to return


class TravelQuestionResponseResponseView(FormView):
	template_name = 'travel_agency/form_views/travel_question_response_response_form.html'
	form_class = forms.TravelQuestionResponseResponseForm

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			return super(TravelQuestionResponseResponseView, self).dispatch(request, *args, **kwargs)

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			return reverse('home:chooser') # what url you wish to return


# User Picture adjsut for modal
class ProfilePicView(FormView):
	template_name = 'travel_agency/profile_pic.html'
	form_class = forms.ProfilePicForm

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			# user_display(request.user)
			return super(ProfilePicView, self).dispatch(request, *args, **kwargs)

	def get_initial(self):
		"""
		Returns the initial data to use for forms on this view.
		"""
		initial = super(ProfilePicView, self).get_initial()

		try:
			userPicture = UserPicture.objects.get(user=self.request.user)
			initial['picture'] = userPicture.picture
			initial['thumbnail'] = userPicture.thumbnail
		except UserPicture.DoesNotExist:
			pass
		return initial

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			rev = 'od=' + self.request.POST.get('open_dialog', 'f')
			return "%s?%s" % (reverse('home:extends'), rev)

	def form_valid(self, form):
		# userPicture, created = UserPicture.objects.get_or_create(user=self.request.user)
		# form = forms.ProfilePicForm(instance=userPicture)
		# userPicture = form.save(commit=False)
		# userPicture.save()
		try:
			userPicture = UserPicture.objects.get(user=self.request.user)
			# UserPicture.objects.filter(user=userPicture.user).update(**form.cleaned_data)
			clean_data = form.cleaned_data
			# clean_data = form.save(commit=false)
			userPicture.picture = clean_data['picture']
			userPicture.thumbnail = clean_data['thumbnail']
		except UserPicture.DoesNotExist:
			userPicture = UserPicture.objects.create(user=self.request.user, **form.cleaned_data)
		userPicture.save()
		return super(ProfilePicView, self).form_valid(form)


# Settings View
class ExtendsView(FormView):
	template_name = 'travel_agency/extends.html'
	form_class = forms.ExtendsForm

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			# user_display(request.user)
			return super(ExtendsView, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(ExtendsView, self).get_context_data(**kwargs)
		open_dialog = self.request.GET.get('od', 'f')
		context['open_dialog'] = open_dialog
		try:
			context['user_picture'] = UserPicture.objects.get(user=self.request.user)
		except UserPicture.DoesNotExist:
			pass
		return context

	def get_initial(self):
		"""
		Returns the initial data to use for forms on this view.
		"""
		initial = super(ExtendsView, self).get_initial()

		try:
			userExtends = UserExtends.objects.get(user=self.request.user)
			# initial['picture'] = userExtends.picture
			# initial['thumbnail'] = userExtends.thumbnail
			initial['sex'] = userExtends.sex
			initial['birthday'] = userExtends.birthday
			initial['phone'] = userExtends.phone
			initial['country'] = userExtends.country
			initial['contact_info'] = userExtends.contact_info
			initial['available_time'] = userExtends.available_time
		except UserExtends.DoesNotExist:
			pass
		return initial

	def get_success_url(self):
		# find your next url here
		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
		if next_url:
			return "%s" % (next_url) # you can include some query strings as well
		else :
			return reverse('home:chooser') # what url you wish to return

	def form_valid(self, form):
		# userExtends, created = UserExtends.objects.get_or_create(user=self.request.user)
		# form = forms.ExtendsForm(instance=userExtends)
		# userExtends = form.save(commit=false)
		# userExtends.user = self.request.user
		try:
			userExtends = UserExtends.objects.get(user=self.request.user)
			UserExtends.objects.filter(user=userExtends.user).update(**form.cleaned_data)
			# userExtends.refresh_from_db()
			# clean_data = form.cleaned_data
			# clean_data = form.save(commit=false)
			# userExtends.picture = clean_data['picture']
			# userExtends.thumbnail = clean_data['thumbnail']
			# userExtends.sex = clean_data['sex']
			# userExtends.birthday = clean_data['birthday']
			# userExtends.phone = clean_data['phone']
			# userExtends.country = clean_data['country']
			# userExtends.contact_info =  clean_data['contact_info']
			# userExtends.available_time = clean_data['available_time']
		except UserExtends.DoesNotExist:
			userExtends = UserExtends.objects.create(user=self.request.user, **form.cleaned_data)
			# userExtends.picture = clean_data['picture']
			# userExtends.thumbnail = clean_data['thumbnail']
			userExtends.save()
		# userExtends, created = UserExtends.objects.get_or_create(user=self.request.user, **form.cleaned_data)
		# if created:
		# else:
		#     UserExtends.objects.filter(user=userExtends.user).update(**form.cleaned_data)
		#     userExtends.refresh_from_db()
		return super(ExtendsView, self).form_valid(form)

# class HopeCityView(FormView):
# 	template_name = 'travel_agency/hope_city.html'
# 	form_class = forms.HopeCityForm

# 	def dispatch(self, request, *args, **kwargs):
# 		if not request.user.is_authenticated():
# 			return redirect('intro')
# 		else:
# 			return super(HopeCityView, self).dispatch(request, *args, **kwargs)

# 	def get_success_url(self):
# 		# find your next url here
# 		next_url = self.request.POST.get('next', None) # here method should be GET or POST.
# 		if next_url:
# 			return "%s" % (next_url) # you can include some query strings as well
# 		else :
# 			return reverse('home:chooser') # what url you wish to return

