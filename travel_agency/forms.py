from django import forms
from django.contrib.auth.models import User
from django.forms.extras.widgets import SelectDateWidget
from django_countries.widgets import CountrySelectWidget
from datetimewidget.widgets import DateTimeWidget

from api.models import UserExtends, UserPicture, TravelInfo, TravelQuestion, TravelInfoResponse, TravelQuestionResponse, TravelQuestionResponseResponse, HopeCity, TravelLocal
from image_cropping import ImageCropWidget
import datetime


class TravelLocalForm(forms.ModelForm):
	class Meta:
		model = TravelLocal
		exclude = ('user', "country_city",)


class TravelInfoForm(forms.ModelForm):
	time_start = forms.DateTimeField(widget=DateTimeWidget(usel10n=True, bootstrap_version=3))
	time_end = forms.DateTimeField(widget=DateTimeWidget(usel10n=True, bootstrap_version=3))

	class Meta:
		model = TravelInfo
		exclude = ('user', "country_city",)


class TravelQuestionForm(forms.ModelForm):
	class Meta:
		model = TravelQuestion
		exclude = ('user', "country_city", "rate", "views")


class TravelInfoResponseForm(forms.ModelForm):
	class Meta:
		model = TravelInfoResponse
		exclude = ('user',)


class TravelQuestionResponseForm(forms.ModelForm):
	class Meta:
		model = TravelQuestionResponse
		exclude = ('user',)


class TravelQuestionResponseResponseForm(forms.ModelForm):
	class Meta:
		model = TravelQuestionResponseResponse
		exclude = ('user',)


class ExtendsForm(forms.ModelForm):
	time = datetime.datetime.now()
	birthday = forms.DateField(
		widget=SelectDateWidget(
			years=range(time.year-50, time.year)
		)
	)
	class Meta:
		model = UserExtends
		exclude = ('user',)


class ProfilePicForm(forms.ModelForm):
	class Meta:
		model = UserPicture
		exclude = ('user',)
		widgets = {
			'picture': ImageCropWidget,
		}


# class HopeCityForm(forms.ModelForm):
# 	class Meta:
# 		model = HopeCity
# 		fields = ('country', 'city', 'more')
#         widgets = {'country': CountrySelectWidget()}

	# def __init__(self, user_id, *args, **kwargs):
	#   self.user_id = user_id
	#   super(ExtendsForm, self).__init__(*args, **kwargs)

	# def save(self, commit=True):
	#   extends = super(ExtendsForm, self).save(commit=False)
	#   extends.user = User.objects.get(pk=self.user_id)

		# userExtends, created = UserExtends.objects.get_or_create(user=self.request.user)
		# form = forms.ExtendsForm(instance=userExtends)
		# userExtends = form.save(commit=false)
		# userExtends.user = self.request.user
		# try:
		#   userExtends = UserExtends.objects.get(user=self.user_id)
		#   UserExtends.objects.filter(user=userExtends.user).update(**form.cleaned_data)
			# userExtends.refresh_from_db()
			# clean_data = form.cleaned_data
			# clean_data = form.save(commit=false)
			# userExtends.picture = self.request.FILES
			# userExtends.sex = clean_data['sex']
			# userExtends.birthday = clean_data['birthday']
			# userExtends.phone = clean_data['phone']
			# userExtends.country = clean_data['country']
			# userExtends.contact_info =  clean_data['contact_info']
			# userExtends.available_time = clean_data['available_time']
		# except UserExtends.DoesNotExist:
		#   userExtends = UserExtends.objects.create(user=self.request.user, **form.cleaned_data)
			# userExtends.picture = self.request.FILES
		#   userExtends.save()
		# userExtends, created = UserExtends.objects.get_or_create(user=self.request.user, **form.cleaned_data)
		# if created:
		# else:
		#     UserExtends.objects.filter(user=userExtends.user).update(**form.cleaned_data)
		#     userExtends.refresh_from_db()
		# if commit:
		#   extends.save()
		# return extends
