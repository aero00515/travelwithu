from django.conf.urls import patterns, include, url

from travel_agency import views

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'travelWithU.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),
	url(r'^$', views.Chooser.as_view(), name="chooser"),
	url(r'^traveler/$', views.Home.as_view(), name="home"),
	# url(r'', include('user_sessions.urls', 'user_sessions')),
	# url(r'', include('django_socketio.urls')),
	# url(r'^node_api/$', views.node_api, name='node_api'),
	# url(r'^chat/$', include('chatrooms.urls')),
	url(r'^settings/$', views.ExtendsView.as_view(), name="extends"),
	url(r'^profile-pic/$', views.ProfilePicView.as_view(), name="pic"),
	url(r'^traveler/main-travel-info/$', views.MainTravelInfoView.as_view(), name="main-travel-info"),
	url(r'^traveler/main-travel-local/$', views.MainTravelLocalView.as_view(), name="main-travel-local"),
	url(r'^traveler/main-travel-question/$', views.MainTravelQuestionView.as_view(), name="main-travel-question"),
	url(r'^traveler/travel-local/$', views.TravelLocalView.as_view(), name="travel-local-form"),
	url(r'^traveler/travel-info/$', views.TravelInfoView.as_view(), name="travel-info-form"),
	url(r'^traveler/travel-question/(?P<pk>[0-9]+)/$', views.TravelQuestionUpdateView.as_view(), name="travel-question-update-form"),
	url(r'^traveler/travel-local/(?P<pk>[0-9]+)/$', views.TravelLocalUpdateView.as_view(), name="travel-local-update-form"),
	url(r'^traveler/travel-info/(?P<pk>[0-9]+)/$', views.TravelInfoUpdateView.as_view(), name="travel-info-update-form"),
	url(r'^traveler/travel-question/$', views.TravelQuestionView.as_view(), name="travel-question-form"),
	url(r'^traveler/travel-info-response/$', views.TravelInfoResponseView.as_view(), name="travel-info-res-form"),
	url(r'^traveler/travel-question-response/$', views.TravelQuestionResponseView.as_view(), name="travel-question-res-form"),
	url(r'^traveler/travel-question-res-res/$', views.TravelQuestionResponseResponseView.as_view(), name="travel-question-res-res-form"),
)
