function onCreateRoomListener(url, user1_email, user2_email) {
	user1 = user1_email.split("@")[0];
	user2 = user2_email.split("@")[0];

	room_name_raw = user1_email + ' & ' + user2_email;
	var room_name = btoa(room_name_raw);
	console.log('room_name_raw: ' + room_name_raw);
	console.log('room_name_len: ' + room_name.length);
	console.log('room_name: ' + room_name);
	data =
		'name=' + room_name + '&' + 
		'slug=' + user1 + '_' + user2 + '&' +
		'email2=' + user2_email;

	$.ajax({
		url: url,
		type: 'POST',
		data: data,
		success:function(data) {
			console.log(data);
		},
		error:function(data) {
			console.log(data);
		}
	})
}