// function getAuthToken() {
// 	$.ajax({
// 		url: '{% url 'api:user-token' %}',
// 		type: 'POST',
// 		headers: {
// 			"X-CSRFToken": getCookie("csrftoken")
// 		},
// 		success: function(data) {
// 			console.log(data);
// 			$('#auth_token').attr('value', data.token);
// 		}
// 	});
// }

tooltip_options = {
	delay: {
		show: 500
	}
};

function getMessage(created_at, first_name, text) {
	return '<div class="row list-group-item" data-toggle="tooltip" data-placement="top" title="'+
				created_at +
				'">' + 
				'<div class="col-md-2">' + 
					first_name +
				'</div>' + 
				'<div class="col-md-8">' +
					text + 
				'</div>' +
				'<div class="col-md-2">' +
				'</div>' + 
			'</div>';
}

$(document).ready(function() {
	var socket = io.connect('inlocal.tw', { autoConnect: true });
	var current_user_id = $('#current_user_id').val();
	var entry_el = $('#message');

	socket.on('connect', function() {
		console.log("connect send current user id for registeration");
		socket.emit('online user', current_user_id, function(data) {
			// TODO if user exists
			console.log('[connect] ' + data);
		});
	});

	socket.on('message', function(message) {
		//Escape HTML characters
		// var data = message.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
		message = JSON.parse(message);
		console.log(message);
		//Append message to the bottom of the list
		$('#messages').append(
			getMessage(message['created_at'], message['first_name'], message['text'])
		);

		// re register tooltip for new time to show
		$('[data-toggle="tooltip"]').tooltip(tooltip_options);

		// scroll to the bottom
		$('#chat_panel').scrollTop($('#chat_panel')[0].scrollHeight);

		entry_el.focus();
	});

	function onMsgSend() {
		// if ($('#to_user_id').val() || $('#room_id').val()) {
		// 	console.log('No room selected!');
		// 	return;
		// }
		var msg = entry_el.val();
		var to_user_id = parseInt($('#to_user_id').val());
		var room_id = parseInt($('#room_id').val());
		var send_message = {
			'comment' : msg,
			'current_user_id' : current_user_id,
			'to_user_id' : to_user_id,
			'room_id' : room_id
		};
		// console.log(send_message);
		if( msg ) {
			socket.emit('send_message', JSON.stringify(send_message), function(data) {
				console.log(data);
			});

			//Clear input value
			entry_el.val('');
		} else {
			console.log('msg: ' + msg);
		}
	}

	$('#btn_submit').click(function() {
		onMsgSend();
	});

	// $('#message_form').submit(function(e) {
	// 	e.preventDefault();
	// 	onMsgSend();
	// });

	entry_el.keypress(function(event) {
		//When enter is pressed send input value to node server
		if(event.keyCode == 13) {
			onMsgSend();
		}
	});

	entry_el.focus(function() {
		getNotifyNumber();
	});

	// Initialize the tooltip for message time
	// 		and scroll the message to bottom
	// 		and set default first room and user 
	console.log('init');
	$('[data-toggle="tooltip"]').tooltip(tooltip_options);
	$('#chat_panel').scrollTop($('#chat_panel')[0].scrollHeight);
	setFirstRoomValue();

	$('#chat_panel').scroll(function() {
		// console.log('scroll n: ' + $('#chat_panel').scrollTop());
		// console.log('scroll e: ' + ($('#chat_panel')[0].scrollHeight - 300));
		if ($('#chat_panel').scrollTop() >= ($('#chat_panel')[0].scrollHeight - 300)) {
			// Readed when scroll bottom, set pending to False
			var room_id = $("#room_id").val();
			var messages_url = '/travel-api/message/?room_id=' + room_id + '&pending=true';
			$.ajax({
				url: messages_url,
				type: 'GET',
				success: function(data) {
					// if (typeof data !== 'undefined' && data.length > 0){
					for (var i in data) {
						console.log(i, data[i]);
						var msg = data[i];
						var message_url = '/travel-api/message/' + msg.id + '/';
						msg.pending = false;
						$.ajax({
							url: message_url,
							type: 'PUT',
							data :msg,
							success: function(data) {
								console.log(data);
							},
							error: function(e) {
								console.log(e);
							}
						});
					}
					// } else {
					// 	getNotifyNumber();
					// }
				}, 
				error: function(e) {
					console.log(e);
				}
			});
		}
	});
});

$('#room_tab a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
});

function onRoomClick(room_id, to_user_id, url) {
	// console.log('Hello: ' + room_id + " to " + to_user_id);
	$("#room_id").val(room_id);
	$("#to_user_id").val(to_user_id);

	// Query room's message with travel-api
	// 1. Query room messages
	// 2. Clear messages for new message 
	// $('#messages').html("");
	// 3. Append messages to #messages with special format
	var message_url = url + '?room_id=' + room_id + '&name=True';
	console.log('query: ' + message_url);
	$.ajax( {
		url: message_url,
		type: 'GET',
		success: function(data) {
			$('#messages').html("");
			for (var i in data) {
				var message = data[i];
				$('#messages').append(
					getMessage(message.created_at, message.user.first_name, message.text)
				);
			}
			$('[data-toggle="tooltip"]').tooltip(tooltip_options);
			$('#chat_panel').scrollTop($('#chat_panel')[0].scrollHeight);
		}
	});
}

// $('#travel_tabs a[href="#find_local"]').tab('show') // Select tab by name
// $('#travel_tabs a:first').tab('show') // Select first tab
// $('#travel_tabs a:last').tab('show') // Select last tab
// $('#travel_tabs li:eq(2) a').tab('show') // Select third tab (0-indexed)