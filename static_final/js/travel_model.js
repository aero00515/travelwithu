$(document).ready(function() {

	// Listen the href clicked
	$('[data-toggle="modal"]').each(function() {
		$(this).click(function() {
			var id = $(this).attr('id');
			var url = $(this).attr('twu-url');
			var kind = id.split('-')[0];
			var pk = id.split('-')[1];
			console.log('id: ' + id);
			console.log('kind: ' + kind);
			console.log('pk: ' + pk);
			if (kind == 'travel_info') {
				// $('#travel_info_frame');
				// $('#info_frame_label').html('info ' + pk);
				$.ajax({
					url: url,
					type: 'GET',
					success: function(data) {
						// console.log(data);
						$('#travel_info_frame').html(data);
					}
				});
			} else if (kind == 'travel_question') {
				$.ajax({
					url: url,
					type: 'GET',
					success: function(data) {
						// console.log(data);
						$('#travel_question_frame').html(data);
					}
				});
				// $('#travel_question_frame');
				// $('#question_frame_label').html('question ' + pk);
			} else if (kind == 'travel_local') {
				$.ajax({
					url: url,
					type: 'GET',
					success: function(data) {
						// console.log(data);
						$('#travel_local_frame').html(data);
					}
				});
			} else {
				// no this entry yet
				// TODO exception
			}
			
		});
	}) 

});