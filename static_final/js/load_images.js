var api = '/travel-api/';
var api_user = api + 'user/';
var api_userpic = api + 'userpicture/';

// function getNavbarHeight() {
// 	// navbar = document.getElementById('navbar');
// 	// return navbar.style.height;
// 	return window.getComputedStyle(
// 		document.getElementById('navbar'), null)
// 		.getPropertyValue('height');
// }

function getNavUserInformation(pk) {
	getProfileUser(pk, api_user + pk + '/');
	getProfilePicture(pk, api_userpic + pk + '/');
}

function getProfileUser(pk, url) {
	$.ajax({
		url: url,
		type: 'GET',
		success: function(data) {
			// console.log(data);
			// console.log(data.picture);
			profile_pic = document.getElementById("#profile_name");
			profile_pic.textContent = data.first_name;
			// profile_pic.src = data.picture;
		}
	});
}

function getProfilePicture(pk, url, id) {
	id = typeof id !== 'undefined' ? id : '#profile_pic';

	$.ajax({
		url: url,
		type: 'GET',
		success: function(data) {
			// console.log(data);
			// console.log(data.picture);
			// console.log(id);
			profile_pic = document.getElementById(id);
			thumbnail = data.thumbnail
			if (typeof thumbnail != 'undefined') {
				profile_pic.src = thumbnail;
			}
			// profile_pic.src = data.picture;
		}
	});
}

function loadImage(img_object, callback) {
	var pk = img_object.data('user-img');
	var picUrl = api_userpic + pk + '/';
	$.ajax({
		url: picUrl,
		type: 'GET',
		success: function(data) {
			callback(data);
		},
		error: function (e) {
			console.log(e);
		}
	});
}

function getImage(id, callback) {
	var picUrl = api_userpic + id + '/';
	$.ajax({
		url: picUrl,
		type: 'GET',
		success: function(data) {
			callback(id, data);
		}, 
		error: function(e) {
			console.log(e);
		}
	});
}

function loadAllImage(callback) {
	// Get all profile pic to set personal pic
	// This way is better than load each to avoid repeat query
	// 1. Select type of 'profile_pic'
	// 2. Get the user "ids" array
	// 3. Query each id's profile_pic's link
	// 4. Set to the exact id's to 'img' src field
	var ids_img = {};
	$('img[data-type="profile_pic"]').each(function(i, v) {
		var img = $(this);
		var id = img.data('id');
		var img_array = [];
		if (id in ids_img) {
			// Already in ids query array
			img_array = ids_img[id];
		}
		img_array.push(img);
		ids_img[id] = img_array;
	});

	for (var id in ids_img) {
		getImage(id, function(r_id, data) {
			ids_img[r_id].forEach(function(img) {
				img.attr('src', data.thumbnail);
			});
		});
	}

	callback('loaded', ids_img);
}