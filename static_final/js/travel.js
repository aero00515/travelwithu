var find_local = 0;
var local_post = 1;
var question = 2;
// var find_partener = 3;

function getState() {
	if (document.getElementById("find_local").className.indexOf("active") > -1) {
		return find_local;
	} else if (document.getElementById("local_post").className.indexOf("active") > -1) {
		return local_post;
	} 
	// else if (document.getElementById("find_partener").className.indexOf("active") > -1) {
	// 	return find_partener;
	// } 
	else if (document.getElementById("question").className.indexOf("active") > -1) {
		return question;
	} else {
		return -1;
	}
}

function addTravel(local_post_url, find_local_url, question_url, country) {
	var currentURL = window.location.href;

	if (getState() == find_local) {
		var redirect_to_url = find_local_url;
	} 
	// else if (getState() == find_partener) {
	// 	var redirect_to_url = find_partener_url;
	// } 
	else if (getState() == local_post) {
		var redirect_to_url = local_post_url;
	} else if (getState() == question) {
		var redirect_to_url = question_url;
	} else {
		// WTF?
		var redirect_to_url = "/";
	}

	// similar behavior as an HTTP redirect
	// window.location.replace(redirect_to_url);

	// similar behavior as clicking on a link
	window.location.href = redirect_to_url + "?country=" + country;
}

$(document).ready(function() {
	// Get notify number
	$.ajax({
		url: '/travel-api/notifycount/',
		type: 'GET',
		success: function(data) {
			var num = parseInt(data);
			if (num > 0) {
				$('[data-type="notify"]').text(data);
			} else if (num > 10) {
				$('[data-type="notify"]').text('10+');
			} else {
				// $('[data-type="notify"]').text(data);
				$('[data-type="notify"]').hide();
			}
		}, 
		error: function(e) {
			console.log(e);
		}
	});
});