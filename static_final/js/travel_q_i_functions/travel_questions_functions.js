function editOrSaveRes(edit, save, response, response_id, url, question_id, user) {
	if ($('#question_res_self').text() == edit) {
		$('#question_res_self').text(save);

		// Let detail can modify
		// var suggestion = '{{ travel_question_response.detail|escapejs }}';
		var suggestion = $('[data-info-type="can_edit_question_detail"]').text();
		if (suggestion === 'undefined') {
			suggestion = '';
		}
		var holder = response;
		var is_con = $('#question_connection_btn_' + response_id).data('checked');
		$('[data-info-type="can_edit_question_detail"]').html('<textarea class="form-control" rows="3" placeholder="' + holder + '">' + suggestion + '</textarea>');

		// Let connection can modify
		$('[data-info-type="can_edit_question_connection"]').html(
			'<input class="form-control" type="checkbox" name="is-c-m-checkbox" checked>'
		);
		$("[name='is-c-m-checkbox']").bootstrapSwitch('state', is_con);
		$("[name='is-c-m-checkbox']").bootstrapSwitch('size', 'mini');
		$("[name='is-c-m-checkbox']").bootstrapSwitch('onText', 'YES');
		$("[name='is-c-m-checkbox']").bootstrapSwitch('offText', 'NO');

		$('[data-info-type="can_edit_question_detail"]').attr(
			'data-info-type', "modify_question_detail"
		);
		$('[data-info-type="can_edit_question_connection"]').attr(
			'data-info-type', "modify_question_connection"
		);

		console.log('Edit: ' + response_id);
	} else {
		$('#question_res_self').text(edit);

		// Get modify value
		// Suggestion
		var new_suggestion = $('[data-info-type="modify_question_detail"]').find('textarea').val();
		console.log(new_suggestion);
		if (new_suggestion === 'undefined') {
			new_suggestion = '';
		}
		$('[data-info-type="modify_question_detail"]').html('<p id="new_suggestion">' + new_suggestion + '</p>');

		// Can connect
		var checked = $('[data-info-type="modify_question_connection"]').find('input').prop('checked');
		console.log(checked);
		var con_class = 'glyphicon glyphicon-share';
		if (checked) {
			con_class = 'glyphicon glyphicon-envelope';
		}
		$('[data-info-type="modify_question_connection"]').html(
			'<button id="question_connection_btn_' + response_id + '" data-checked=' + checked + ' class="' + con_class + '">' + '</button>'
		);

		// Save and update
		$.ajax({
			url: url,
			type: 'PUT',
			data: {
				travel_question: question_id,
				user: user,
				is_connection: false, // checked
				detail: $('#new_suggestion').text()
				// "rate": 0,
				// "is_choose": false,
			},
			success: function(data) {
				console.log(data);
			}, 
			error: function(data) {
				console.log(data);
			}
		});

		$('[data-info-type="modify_question_detail"]').attr('data-info-type', "can_edit_question_detail");
		$('[data-info-type="modify_question_connection"]').attr('data-info-type', "can_edit_question_connection");

		console.log('Save: ' + response_id);
	}
}

function sendQuestionResponse(url, travel_question, user) {
	// var res_is_connected = $("#is_question_connected").bootstrapSwitch('state');
	var res_is_connected = false;
	var res_detail = $('#travel_question_response_textarea').val();
	if (res_detail === 'undefined') {
		// TODO notify the user to enter response
		console.log('TODO notify the user to enter response');
	} else {
		// TODO check is update(put) or create(post)
		// no user, create
		$.ajax({
			url: url,
			type: 'POST',
			data: {
				travel_question: travel_question,
				user: user,
				is_connection: res_is_connected,
				detail: res_detail
				// "rate": 0,
				// "is_choose": false,
			},
			success: function(data) {
				console.log(data);
				// $('#travel_question_response_textarea').val('');
			}, 
			error: function(data) {
				console.log(data);
			}
		});
	}
	console.log(res_is_connected);
	console.log(res_detail);
}

$(document).ready(function() {

	function resres(text) {
		var isShown = text.data('res-show');
		// console.log(isShown);
		if(isShown) {
			text.show('700');
		} else {
			text.hide('700');
		}
	}

	// Do Question response's response
	$('[data-type="question-res-res"]').each(function(index, value) {
		// console.log(index + ":", value);
		var q_res_res = $(this);
		var q_res_res_text_frame = q_res_res.find('[data-type="question-res-res-text-frame"]');
		var q_res_res_text_area = q_res_res.find('textarea');
		var q_res_res_send_button = q_res_res.find('button');

		resres(q_res_res_text_frame);

		var res_id = q_res_res.data('res-id');
		var user_id = q_res_res.data('user-id');
		var post_url  = q_res_res.data('post-url');
		// console.log("res id: " + res_id);

		// Make Response click show/hide the textarea
		q_res_res.find('a').click(function() {
			// console.log("click: " + res_id);
			// TODO add textarea frame for user to response
			var isShown = q_res_res_text_frame.data('res-show');
			// console.log(isShown);
			q_res_res_text_frame.data('res-show', !isShown);
			resres(q_res_res_text_frame);
		});

		// Send res-res button
		q_res_res_send_button.click(function() {
			var value = q_res_res_text_area.val();
			// console.log(value);

			$.ajax({
				url: post_url,
				type: 'POST',
				data: {
					travel_question_response: res_id,
					user: user_id,
					detail: value
				},
				success: function(data) {
					console.log(data);
					// $('#travel_question_response_textarea').val('');
				}, 
				error: function(data) {
					console.log(data);
				}
			});
		});

		// Enable/disable the send res-res button
		q_res_res_text_area.bind('input propertychange', function() {
			var value = q_res_res_text_area.val();
			// console.log(value);
			if (value !== 'undefined', value != '') {
				q_res_res_send_button.attr('disabled', false);
			} else {
				q_res_res_send_button.attr('disabled', true);
			}
		});

	});
});