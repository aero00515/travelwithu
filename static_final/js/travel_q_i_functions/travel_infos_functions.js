function editOrSaveInfoRes(edit, save, response, response_id, url, info_id, user) {
	if ($('#info_res_self').text() == edit) {
		$('#info_res_self').text(save);

		// Suggestion
		var suggestion = $('[data-info-type="can_edit_info_suggestion"]').text();
		if (suggestion === 'undefined') {
			suggestion = '';
		}
		var holder = response;
		var is_con = $('#info_connection_btn_' + response_id).data('checked');
		$('[data-info-type="can_edit_info_suggestion"]').html('<textarea class="form-control" rows="3" placeholder="' + holder + '">' + suggestion + '</textarea>');

		// Connect
		$('[data-info-type="can_edit_info_connection"]').html(
			'<input class="form-control" type="checkbox" name="is-c-m-checkbox" checked>'
		);
		$("[name='is-c-m-checkbox']").bootstrapSwitch('state', is_con);
		$("[name='is-c-m-checkbox']").bootstrapSwitch('size', 'mini');
		$("[name='is-c-m-checkbox']").bootstrapSwitch('onText', 'YES');
		$("[name='is-c-m-checkbox']").bootstrapSwitch('offText', 'NO');

		$('[data-info-type="can_edit_info_suggestion"]').attr('data-info-type', "modify_info_suggestion");
		$('[data-info-type="can_edit_info_connection"]').attr('data-info-type', "modify_info_connection");

		console.log('Edit: ' + response_id);

	} else {
		// Save and update
		// get modify value
		$('#info_res_self').text(edit);
		// Suggestion
		var new_suggestion = $('[data-info-type="modify_info_suggestion"]').find('textarea').val();
		console.log(new_suggestion);
		if (new_suggestion === 'undefined') {
			new_suggestion = '';
		}
		$('[data-info-type="modify_info_suggestion"]').html('<p id="new_suggestion">' + new_suggestion + '</p>');

		// Can connect
		var checked = $('[data-info-type="modify_info_connection"]').find('input').prop('checked');
		console.log(checked);
		var con_class = 'glyphicon glyphicon-share';
		if (checked) {
			con_class = 'glyphicon glyphicon-envelope';
		}
		$('[data-info-type="modify_info_connection"]').html(
			'<button id="info_connection_btn_' + response_id + '" data-type="info_connection_btn" data-to="' + user + '" data-res="' + response_id + '" data-checked=' + checked + ' class="' + con_class + '">' + '</button>'
		);
		$.ajax({
			url: url,
			type: 'PUT',
			data: {
				travel_info: info_id,
				user: user,
				is_connection: checked,
				suggestion: $('#new_suggestion').text()
			},
			success: function(data) {
				console.log(data);
			}, 
			error: function(data) {
				console.log(data);
			}
		});

		$('[data-info-type="modify_info_suggestion"]').attr('data-info-type', "can_edit_info_suggestion");
		$('[data-info-type="modify_info_connection"]').attr('data-info-type', "can_edit_info_connection");

		console.log('Save: ' + response_id);
	}
}

function sendInfoResponse(url, travel_info, user) {
	var res_is_connected = $("#is_info_connected").bootstrapSwitch('state');
	var res_detail = $('#travel_info_response_textarea').val();
	if (res_detail === 'undefined') {
		// TODO notify the user to enter response
		console.log('TODO notify the user to enter response');
	} else {
		// TODO check is update(put) or create(post)
		// no user, create
		$.ajax( {
			url: url,
			type: 'POST',
			data: {
				travel_info: travel_info,
				user: user,
				is_connection: res_is_connected,
				suggestion: res_detail
			},
			success: function(data) {
				console.log(data);
				// $('#travel_info_response_textarea').val('');
			}, 
			error: function(data) {
				console.log(data);
			}
		});
	}
	console.log(res_is_connected);
	console.log(res_detail);
}