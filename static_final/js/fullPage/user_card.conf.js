$(document).ready(function() {

    // var is_click_vis = false;
    $('#local_handle').click(function() {
        // Set the effect type
        var effect = 'slide';

        // Set the options for the effect type chosen
        var options = { direction: 'right' };

        // Set the duration (default: 400 milliseconds)
        var duration = 400;

        $('#local_menu').toggle(effect, options, duration);
        // is_click_vis = $('#local_menu').is(':visible');
    });

    $('#about_handle').click(function() {
        $.fn.fullpage.moveTo(2);
    });

    $('#fullpage').fullpage({
        // Navigation
        // 'navigation': true,
        // 'navigationPosition': 'left',
        'resize':true,
        // 'paddingBottom': getNavbarHeight(),
        // slidesNavigation: true,
        // slidesNavPosition: 'top'
        // anchors: ['systemModel', 'fuzzySystem', 'controller', 'fuzzyController', 'ga', 'lastPage'],
        // anchors: ['Intro', 'Data', 'Demo', 'lastPage'],
        // menu: '#menu',

        // Scrolling
        // loopHorizontal: false,
        // scrollingSpeed: 500,
        scrollOverflow: true,

        //Design
        // controlArrows: false
        // sectionsColor: ['#f2f2f2', '#4BBFC3', '#7BAABE', 'whitesmoke', '#000', '#666'],
        // sectionsColor: ['#f2f2f2', '#4BBFC3', '#666'],

        // onSlideLeave: function (anchorLink, index, slideIndex, direction) {
        //   console.log('onSlideLeave');
        //   //For the section 1, slide 0 to the right...
        //   if (index == 1 && slideIndex == 0 && direction == 'right') {
        //     console.log('For the section 1, slide 0 to the right...');
        //     var section2 = $('#section1');
        //     var section3 = $('#section2');
        //     $.fn.fullpage.setScrollingSpeed(0);
        //     $.fn.fullpage.scrollSlider(section2, 1);
        //     $.fn.fullpage.scrollSlider(section3, 1);
        //     $.fn.fullpage.setScrollingSpeed(700);
        //   }
        // }

        onLeave: function(index, nextIndex, direction) {

            // onLeave
            if (index == 1) {
                // Not at top, dismiss local handle
                // if (!is_click_vis) {
                    $('#local_menu').toggle(false);
                // }
            } else if (index == 2  && direction =='up') {
                // At top, show local handle
                // is_click_vis = false;
                // if (!is_click_vis) {
                    $('#local_menu').toggle(true);
                // }
            }
            // var leavingSection = $(this);

            //after leaving section 2
            // if(index == 2 && direction =='down'){
            //  alert("Going to section 3!");
            // }

            // else if(index == 2 && direction == 'up'){
            //  alert("Going to section 1!");
            // }
        }
    });
});