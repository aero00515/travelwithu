from django.views import generic
from django.shortcuts import redirect

from api.models import UserExtends, UserPicture, LanguageLevel, TravelLocal, Education, Career, Interest, GoodAt

# Create your views here.
class Home(generic.TemplateView):
	template_name = 'travel_usercard/home.html'

	def dispatch(self, request, *args, **kwargs):
		if not request.user.is_authenticated():
			return redirect('intro')
		else:
			return super(Home, self).dispatch(request, *args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(Home, self).get_context_data(**kwargs)
		user = self.request.user

		context['current'] = {'title' : 'None'}
		try:
			context['user_extends'] = UserExtends.objects.get(user=user)
		except UserExtends.DoesNotExist:
			pass

		try:
			context['user_picture'] = UserPicture.objects.get(user=user)
		except UserPicture.DoesNotExist:
			pass

		try:
			context['current'] = Career.objects.get(user=user, is_current=True)
		except Career.DoesNotExist:
			try:
				context['current'] = {'title' : 'Student'}
				context['education'] = Education.objects.get(user=user, is_current=True)
			except Education.DoesNotExist:
				try:
					context['education'] = Education.objects.filter(user=user).latest('end_at')
				except Education.DoesNotExist:
					pass

		# context['educations'] = Education.objects.filter(user=user)
		# context['careers'] = Career.objects.filter(user=user)
		context['interests'] = Interest.objects.filter(user=user)
		context['goodats'] = GoodAt.objects.filter(user=user)
		context['language_levels'] = LanguageLevel.objects.filter(user=user)
		context['travel_locals'] = TravelLocal.objects.filter(user=user)
		return context