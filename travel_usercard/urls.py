from django.conf.urls import patterns, include, url

from travel_usercard import views

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'travelWithU.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),
	url(r'^$', views.Home.as_view(), name="home"),
	# url(r'^traveler/$', views.Home.as_view(), name="home"),
)
