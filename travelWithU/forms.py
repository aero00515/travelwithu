from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.forms.extras.widgets import SelectDateWidget

from account import forms as ac_forms
from account.conf import settings
from collections import OrderedDict


class SignupForm(ac_forms.SignupForm):

	name = forms.CharField(max_length=254)

	def __init__(self, *args, **kwargs):
		super(SignupForm, self).__init__(*args, **kwargs)
		del self.fields["username"]
		fields_key_order = ['name', 'email', 'password', 'password_confirm']

		if (self.fields.has_key('keyOrder')):
			self.fields.keyOrder = fields_key_order
		else:
			self.fields = OrderedDict((k, self.fields[k]) for k in fields_key_order)


# class SettingForm(forms.ModelForm):
# 	timezone = forms.ChoiceField(
# 		label=_("Timezone"),
# 		choices=[("", "---------")] + settings.ACCOUNT_TIMEZONES,
# 		required=False
# 	)
# 	if settings.USE_I18N:
# 		language = forms.ChoiceField(
# 			label=_("Language"),
# 			choices=settings.ACCOUNT_LANGUAGES,
# 			required=False
# 		)
# 	def __init__(self, *args, **kwargs):
# 		super(SettingForm, self).__init__(*args, **kwargs)
# 		instance = getattr(self, 'instance', None)
# 		if instance and instance.pk:
# 			self.fields['email'].widget.attrs['readonly'] = True

# 	def clean_email(self):
# 		# super(SettingForm, self).clean_email(*args, **kwargs)
# 		instance = getattr(self, 'instance', None)
# 		if instance and instance.pk:
# 			return instance.email
# 		else:
# 			return self.cleaned_data['email']

# 	class Meta:
# 		model = User
# 		fields = ('email', 'timezone', 'language')