from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from travelWithU import views

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'travelWithU.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),
	
	url(r'^intro/$', views.IntroView.as_view(), name="intro"),
	url(r'^invite/$', views.invite_send, name="invite"),
	url(r'^', include('travel_agency.urls', namespace="home")),
	url(r'^user/', include('travel_usercard.urls', namespace="user")),
	url(r'^messages/', include('travel_messenger.urls', namespace="message")),
	url(r'^travel-api/', include('api.urls', namespace="api")),
	url(r'^ckeditor/', include('ckeditor.urls')),
	# url(r'^messages/', include('postman.urls')),
	# url(r'^chat/', include('chatroom.urls')),
	url(r'^account/login/$', views.LoginView.as_view(), name="account_login"),
	url(r'^account/signup/$', views.SignupView.as_view(), name="account_signup"),
	# url(r'^account/languages/$', views.SettingsView.as_view(), name="account_settings"),
	url(r"^account/", include("account.urls")),
	url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
	urlpatterns += patterns('',
		url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
			'document_root': settings.MEDIA_ROOT,
		}),
	)