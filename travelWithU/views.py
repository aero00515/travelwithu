from django.views import generic
# from django.contrib.auth import login
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse

from account import views as ac_views
from account import forms as ac_forms
from account.utils import user_display
from datetime import datetime

from travelWithU import forms, utils, settings
from api.models import InviteRelation

@require_http_methods(['POST'])
def invite_send(request):
	user = request.user
	email = request.POST.get('email')
	try:
		ir = InviteRelation.objects.get(email=email)
		msg = 'already exists: ' + str(ir.pk)
	except InviteRelation.DoesNotExist:
		invite_code = utils.id_generator()
		ir = InviteRelation(user=user, email=email, invite_code=invite_code)
		ir.save()
		invite_url = request.build_absolute_uri(reverse('account_signup')) + "?invite_code=" + invite_code
		email_message = 'Welcome, you have been invited by ' + user.first_name + '.\n'
		email_message += 'Please click following url to signup: \n'
		email_message += invite_url + '\n\n'
		email_message += '[Notice]'
		msg = 'saved: ' + str(ir.pk)
		# send email and url
		send_mail('Step Invitation', email_message, 'aero00515@gmil.com', [email], fail_silently = False)
	return HttpResponse(msg)

class IntroView(generic.TemplateView):
	template_name = 'travelWithU/intro.html'

	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated():
			user_display(request.user)
		return super(IntroView, self).dispatch(request, *args, **kwargs)


class LoginView(ac_views.LoginView):
	form_class = ac_forms.LoginEmailForm


class SignupView(ac_views.SignupView):

	form_class = forms.SignupForm

	# def get_context_data(self, **kwargs):
	# 	ctx = super(SignupView, self).get_context_data(**kwargs)
	# 	invite_code = self.request.GET.get('invite_code')
	# 	if invite_code:
	# 		ctx['invite_code'] = InviteRelation.objects.get(invite_code=invite_code)
	# 	else:
	# 		# tell user to deny
	# 		ctx['error'] = 'Not Invited!'
	# 	return ctx

	def generate_username(self, form):
		# do something to generate a unique username (required by the
		# Django User model, unfortunately)
		# from django.utils.crypto import get_random_string
		username = utils.getSHA224(str(datetime.now()))
		return username

	def user_credentials(self):
		self.identifier_field = 'email'
		return super(SignupView, self).user_credentials()

	def after_signup(self, form):
		self.create_profile(form)
		# if self.request.user.is_active:
		# 	login(self.request, self.request.user)
		super(SignupView, self).after_signup(form)

	def create_profile(self, form):
		user = User.objects.get(pk=self.created_user.pk)
		user.first_name = form.cleaned_data["name"]
		user.save()
		# ir = InviteRelation.objects.get(email=user.email)
		# ir.is_invited = True
		# ir.save()
		# if user.is_active:
		# 	login(self.request, user)

# class SettingsView(ac_views.SettingsView):
# 	form_class = forms.SettingForm