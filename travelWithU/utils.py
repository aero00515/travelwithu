import string
import random
import hashlib
import base64

def getHashMD5(value):
	m = hashlib.md5()
	m.update(value)
	return m.digest()

def getSHA224(value):
	return hashlib.sha224(value).hexdigest()

def getBase64(value):
	encoded = base64.b64encode(b'%s' % value)
	return encoded

def id_generator(size=20, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.SystemRandom().choice(chars) for _ in range(size))