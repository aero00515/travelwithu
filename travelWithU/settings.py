"""
Django settings for travelWithU project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

from easy_thumbnails.conf import Settings as thumbnail_settings

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'bamabpkciuj*8j3k&heg4t_t++su&a+=c^&i)!z@!xp)wi09yn'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'django.contrib.sites',
	'django_countries',

	'django_jenkins',
	'ckeditor',

	# Chat room using nodejs, 
	# The problem of using socketio is that 
	# the session of django cannot send to the nodejs server
	# 'django_socketio',

	'rest_framework',
	'rest_framework.authtoken',
	# 'registration',
	# 'pagination',
	# 'user_sessions',
	# 'polymorphic',

	# Chat room uses ajax pull request every 8 ms, 
	# the problem is now stuck at gevent.event wait cannot switch to a different thread.
	# 'chatroom', 
	# 'postman',
	# 'haystack',
	'bootstrapform',
	'account',
	'easy_thumbnails',
	'image_cropping',
	# 'pinax_theme_bootstrap',
	'travel_usercard',
	'travel_agency',
	'travel_messenger',
	'datetimewidget',
	'api',
)

MIDDLEWARE_CLASSES = (
	'django.contrib.sessions.middleware.SessionMiddleware',
	# 'user_sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
	'django.contrib.auth.backends.ModelBackend',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	"account.middleware.LocaleMiddleware",
	"account.middleware.TimezoneMiddleware",
)

# SESSION_ENGINE = 'user_sessions.backends.db'

ROOT_URLCONF = 'travelWithU.urls'

WSGI_APPLICATION = 'travelWithU.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.sqlite3',
		'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
	# },
	# 'slave': {
		# 'ENGINE': 'django.db.backends.mysql',
		# 'OPTIONS': {
		# 	'read_default_file': os.path.join(BASE_DIR, 'mysql.cnf'),
		# },
	}
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'
# LANGUAGE_CODE = 'zh'

LOCALE_PATHS = (os.path.join(BASE_DIR, 'local'),)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


TEMPLATE_DIRS = (
	BASE_DIR + '/templates/',
)

TEMPLATE_CONTEXT_PROCESSORS = (
	"django.core.context_processors.request",
	"django.core.context_processors.debug",
	"django.core.context_processors.i18n",
	"django.core.context_processors.media",
	"django.core.context_processors.static",
	"django.core.context_processors.tz",
	"django.contrib.auth.context_processors.auth",
	"django.contrib.messages.context_processors.messages",
	# "postman.context_processors.inbox",
	"account.context_processors.account",
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, "static_final")

STATICFILES_DIRS = (
	os.path.join(BASE_DIR, "static"),
)


MEDIA_ROOT = BASE_DIR + "/media/"

MEDIA_URL = "/media/"


CKEDITOR_UPLOAD_PATH = MEDIA_ROOT + "cked/"

CKEDITOR_CONFIGS = {
	'default': {
		'toolbar': 'Custom',
		'toolbar_Custom': [
			['Undo', 'Redo'],
			['Font', 'FontSize'],
			['Bold'],
			['NumberedList', 'BulletedList'],
			['Link', 'Unlink'],
			[ 'Image', 'HorizontalRule'],
			['RemoveFormat']
			# ['Bold', 'Italic', 'Underline'],
			# ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
			# ['Link', 'Unlink'],
			# ['RemoveFormat', 'Source']
		],
		'height': 'auto',
		'width': 'auto'
	},
	'response': {
		'toolbar': 'Custom',
		'toolbar_Custom': [
			['Undo', 'Redo'],
			['Bold'],
			['NumberedList', 'BulletedList'],
			['Link', 'Unlink', 'HorizontalRule'],
			['RemoveFormat']
		],
		'height': 'auto',
		'width': 'auto'
	}
}

## Rest framework settings
REST_FRAMEWORK = {
	# Use Django's standard `django.contrib.auth` permissions,
	# or allow read-only access for unauthenticated users.
	'DEFAULT_PERMISSION_CLASSES': [
		'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
	]
}

ACCESS_TOKEN_EXPIRE_HOUR = 24


## POSTMAN Settings
POSTMAN_DISALLOW_ANONYMOUS = True
POSTMAN_DISABLE_USER_EMAILING = True
POSTMAN_SHOW_USER_AS = 'first_name'
# POSTMAN_DISALLOW_MULTIRECIPIENTS = True  # default is False
# POSTMAN_DISALLOW_COPIES_ON_REPLY = True  # default is False
POSTMAN_AUTO_MODERATE_AS = True  # default is None
# POSTMAN_QUICKREPLY_QUOTE_BODY = True  # default is False
# POSTMAN_NOTIFIER_APP = None  # default is 'notification'
# POSTMAN_MAILER_APP = None  # default is 'mailer'
# POSTMAN_AUTOCOMPLETER_APP = {
	# 'name': '',  # default is 'ajax_select'
	# 'field': '',  # default is 'AutoCompleteField'
	# 'arg_name': '',  # default is 'channel'
	# 'arg_default': 'postman_friends',  # no default, mandatory to enable the feature
# }  # default is {}


## Django Account Settings
# SITE_NAME = "TravelWithU"
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_CONFIRMATION_EMAIL = False
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = False
ACCOUNT_USE_AUTH_AUTHENTICATE = True

ACCOUNT_USER_DISPLAY = lambda user: user.email
ACCOUNT_SIGNUP_REDIRECT_URL = '/'

AUTHENTICATION_BACKENDS = (
	'account.auth_backends.EmailAuthenticationBackend',
)

## Django Mailer 
# EMAIL_BACKEND = "mailer.backend.DbBackend"


## Thumbnail
THUMBNAIL_PROCESSORS = (
	'image_cropping.thumbnail_processors.crop_corners',
) + thumbnail_settings.THUMBNAIL_PROCESSORS

IMAGE_CROPPING_SIZE_WARNING = True


## Django contries
COUNTRIES_FLAG_URL = 'flags/{code}.gif'

IS_INVITE_USAGE = True

# TODO change to the SSL connection with Google Apps 
# Reason is for Google to handle the company email
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587 #465 or 587
EMAIL_USE_TLS = True
from travelWithU.email_settings import *

## 
# HAYSTACK_CONNECTIONS = {
#     'default': {
#         'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
#         'URL': 'http://127.0.0.1:9200/',
#         'INDEX_NAME': 'haystack',
#     },
# }


## Socket io
# SOCKETIO_HOST = '127.0.0.1'
# SOCKETIO_PORT = '8000'


## Jenkins Settings
# PROJECT_APPS = 

# JENKINS_TASKS = (
# 	'django_jenkins.tasks.run_pylint',
# 	'django_jenkins.tasks.with_coverage',
# 	'django_jenkins.tasks.django_tests',
# )

# JENKINS_TEST_RUNNER='django_jenkins.nose_runner.CINoseTestSuiteRunner'